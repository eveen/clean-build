#!/bin/sh

TARGET_DIR=target/clean-lib-tcpip/Libraries/TCPIP

#Visual studio compiler 
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
COMMAND=`cygpath --unix $COMSPEC`

vscc() { # $1: C .c file path

	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.obj
	(cd "$vscc_dir"
	 cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@cl /nologo /GS- /c "$vscc_file" /Fo"$vscc_object_file"
EOBATCH
	)
}

# Copy Clean modules
mkdir -p $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPIP.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPIP.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPDef.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPDef.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPEvent.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPEvent.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannelClass.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannelClass.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannels.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannels.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannels.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannels.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannelsInternal.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannelsInternal.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/tcp_bytestreams.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/tcp_bytestreams.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/tcp.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/tcp.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/ostcp.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/ostcp.icl $TARGET_DIR

mkdir -p $TARGET_DIR/Clean\ System\ Files/
cp src/clean-libraries-master/Libraries/TCPIP/Clean\ System\ Files/wsock_library $TARGET_DIR/Clean\ System\ Files/

# Build and copy C interface
mkdir -p build/TCPIP/csf


#Additional headers
cp src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/OS\ Windows/Windows_C_12/*.h build/TCPIP/csf/
cp src/clean-tools-trunk/htoclean/Clean.h build/TCPIP/csf/

cp src/clean-libraries-master/Libraries/TCPIP/Windows_C/*.c build/TCPIP/csf/
cp src/clean-libraries-master/Libraries/TCPIP/Windows_C/*.h build/TCPIP/csf/

(cd build/TCPIP/csf
     vscc cTCP_121.c
     vscc cCrossCallTCP_121.c
)

mkdir -p $TARGET_DIR/Clean\ System\ Files/
cp build/TCPIP/csf/cTCP_121.obj $TARGET_DIR/Clean\ System\ Files/
cp build/TCPIP/csf/cCrossCallTCP_121.obj $TARGET_DIR/Clean\ System\ Files/

