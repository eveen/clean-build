#!/bin/sh

#Visual studio compiler (only included for building temporary ObjectIO)
VSROOT="C:\Program Files\Microsoft Visual Studio 14.0"
VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
VSVARS="$VSROOT\VC\bin\vcvars32.bat"
VSCC="$VSROOT\VC\bin\cl.exe"
COMMAND=`cygpath --unix $COMSPEC`

mkdir -p build/CleanIDE
cp -r src/clean-ide-master/* build/CleanIDE

#Compile resource
mkdir -p build/idersrc
cp -r src/clean-ide-master/WinSupport/* build/idersrc/
(cd "build/idersrc"
	cat <<EOBATCH | "$COMMAND"
@		"$VSVARS"
@		call makeresource.bat
EOBATCH
)
cp "build/idersrc/winIde.rsrc"  "build/CleanIDE/WinSupport/winIde.rsrc"

export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH

#Compile the tools
(cd "build/CleanIDE"
     ../clean/cpm.exe WinIde.prj
     ../clean/cpm.exe WinTimeProfile.prj
     ../clean/cpm.exe WinHeapProfile.prj
)
TARGET=target/clean-ide

mkdir -p $TARGET
cp build/CleanIDE/CleanIDE.exe $TARGET/
mkdir -p $TARGET/Tools/Time\ Profiler
cp build/CleanIDE/ShowTimeProfile.exe $TARGET/Tools/Time\ Profiler/
mkdir -p $TARGET/Tools/Heap\ Profiler
cp build/CleanIDE/ShowHeapProfile.exe $TARGET/Tools/Heap\ Profiler/

# Copy default configuration files
mkdir -p $TARGET/Config
cp src/clean-ide-master/Config/windows.km $TARGET/Config/default.km
cp src/clean-ide-master/Config/IDEPrefs $TARGET/Config/IDEPrefs

# Copy the relevant help files
mkdir -p $TARGET/Help
cp src/clean-ide-master/Help/UserManual.pdf $TARGET/Help/UserManual.pdf

