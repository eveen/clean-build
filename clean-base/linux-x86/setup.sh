#!/bin/bash
mkdir -p dependencies
curl -L -o dependencies/bootstrap-clean.tgz https://ftp.cs.ru.nl/Clean/Clean30/linux/clean3.0_32.tar.gz
(cd dependencies
	tar -xzf bootstrap-clean.tgz
	mv clean bootstrap-clean
	(cd bootstrap-clean; make)
)

# Setup the initial clean system
rm -rf build
mkdir -p build/clean
cp -r dependencies/bootstrap-clean/* build/clean/
