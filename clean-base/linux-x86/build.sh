#!/bin/sh
set -e

export CLEAN_HOME=`pwd`/build/clean
export CLEANPATH=`pwd`/build/clean/lib/StdEnv
export CLEANLIB=`pwd`/build/clean/lib/exe
export PATH=`pwd`/build/clean/bin:$PATH

PACKAGE=$1
OS=$2
ARCH=$3

#### Component build functions ####

build_codegenerator () { # $1:target 
	mkdir -p build
	cp -r src/code-generator-master build/codegenerator
	make -C build/codegenerator -f Makefile.linux cg

	mkdir -p $1/lib/exe
	cp build/codegenerator/cg $1/lib/exe
}

build_runtimesystem_c () { # $1:target
	mkdir -p build
	cp -r src/run-time-system-master build/runtimesystem

	mkdir -p $1/lib/StdEnv/Clean\ System\ Files

	make -B -C build/runtimesystem -f Makefile.linux
	cp build/runtimesystem/_startup.o $1/lib/StdEnv/Clean\ System\ Files/

	make -B -C build/runtimesystem -f Makefileprofile.linux
	cp build/runtimesystem/_startupProfile.o $1/lib/StdEnv/Clean\ System\ Files/
	touch $1/lib/StdEnv/_startupProfile.dcl

	make -B -C build/runtimesystem -f Makefiletrace.linux
	cp build/runtimesystem/_startupTrace.o $1/lib/StdEnv/Clean\ System\ Files/
	touch $1/lib/StdEnv/_startupTrace.dcl
}

build_runtimesystem_clean () { # $1:target
	#Generate the 'clean part' of the runtime system
	mkdir -p $1/lib/StdEnv/Clean\ System\ Files
	#Use the new code generator
	target/clean-c-components/lib/exe/cg $1/lib/StdEnv/Clean\ System\ Files/_system
}

build_clm () { # $1:target
	mkdir -p build
	cp -r src/clm-master build/clm
	make -C build/clm -f Makefile.linux clms
	mkdir -p $1/bin
	cp build/clm/clms $1/bin/clm
}

build_cpm () { # $1:target
	#Collect dependencies
	mkdir -p build/cpm/deps
	cp -r src/clean-libraries-master/Libraries/StdLib build/cpm/deps/stdlib

	#Get cpm source code
	cp -r src/clean-ide-master build/cpm/CleanIDE
	(cd build/cpm/CleanIDE/cpm
		clm -h 256M -nr -nt -I Posix -I ../BatchBuild -I ../Pm -I ../Unix -I ../Unix/Intel -I ../Util -I ../Interfaces/LinkerInterface\
			-I ../../deps/stdlib -IL ArgEnv -IL Directory -IL Generics -l -m32 Cpm -o cpm
	)

	mkdir -p $1/bin
	cp build/cpm/CleanIDE/cpm/cpm $1/bin/cpm

	mkdir -p $1/etc
	mkdir -p $1/Temp
	cp $PACKAGE/$OS-$ARCH/txt/IDEEnvs $1/etc/IDEEnvs
}

build_compiler () { # $1:target $2:extra flags
	mkdir -p build
	cp -r src/compiler-master build/compiler
	(cd build/compiler
		clm -ABC -nw -ci -I backend -I frontend -I main/Unix -IL ArgEnv backendconvert
		sed -i "s/32M/256M $2/" unix/make.linux.sh
		./unix/make.linux.sh
	)
	mkdir -p $1/lib/exe
	cp build/compiler/cocl $1/lib/exe/cocl
}

build_compiler_itask () { # $1:target $2:extra flags
	mkdir -p build
	cp -r src/compiler-itask build/compiler-itask
	(cd build/compiler-itask
		clm -ABC -nw -ci -I backend -I frontend -I main/Unix -IL ArgEnv backendconvert
		sed -i "s/60M/256M $2/" unix/make.linux.sh
		./unix/make.linux.sh
	)
	mkdir -p $1/lib/exe
	cp build/compiler-itask/cocl $1/lib/exe/cocl-itasks
}

build_docs () { #$1:target
	cp src/clean-ide-master/CleanLicenseConditions.txt $1/CleanLicenseConditions.txt
	cp $PACKAGE/$OS-$ARCH/txt/README $1/README.md

	mkdir -p build
	cp -r src/language-report-master/write_clean_manual build/manual
	(cd build/manual
		#Create generator
		clm -h 20M -l -m32 write_clean_manual -o write_clean_manual

		#Copy fonts
		cp ../../Fonts/LiberationSans-Regular.ttf .
		cp ../../Fonts/LiberationSans-Bold.ttf .
		cp ../../Fonts/LiberationSans-Italic.ttf .
		cp ../../Fonts/NimbusMonoPS-Regular.ttf .
		cp ../../Fonts/NimbusMonoPS-Bold.ttf .

		#Generate manual
		./write_clean_manual
	)
	#Copy to target
   	mkdir -p $1/doc
   	cp build/manual/CleanLanguageReport.* $1/doc/
}

build_stdenv() { # $1:target
	mkdir -p $1/lib/StdEnv
	cp src/stdenv-master/_library.dcl $1/lib/StdEnv/
	cp src/stdenv-master/_startup.dcl $1/lib/StdEnv/
	cp src/stdenv-master/_system.dcl $1/lib/StdEnv/

	for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
		StdFile StdFunc StdFunctions StdInt StdList StdMisc StdOrdList StdOverloaded \
		StdOverloadedList StdReal StdStrictLists StdString StdTuple StdMaybe \
		_SystemArray _SystemEnum _SystemEnumStrict _SystemStrictLists _SystemStrictMaybes StdGeneric;
	do cp src/stdenv-master/$a.[di]cl $1/lib/StdEnv ;
	done

	mkdir -p $1/lib/StdEnv/Clean\ System\ Files
	cp src/stdenv-master/Clean\ System\ Files/_system.abc $1/lib/StdEnv/Clean\ System\ Files

	if [ ! -z ${BUILD_STDENV+x} ]; then
		echo first compile of system modules
		for a in StdChar; # compile twice for inlining
		do $CLEAN_HOME/lib/exe/cocl -ou -pm -pt -P $1/lib/StdEnv $a ;
		done

		echo second compile of system modules
		for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
		do $CLEAN_HOME/lib/exe/cocl -ou -pm -pt -P $1/lib/StdEnv $a ;
		done
	fi
}

build_argenv() { # $1:target
	mkdir -p build/libraries/
	cp -r src/clean-libraries-master/Libraries/ArgEnvUnix build/libraries/ArgEnv
	(cd build/libraries/ArgEnv
		cc -m32 -Wall -pedantic -O -c ArgEnvC.c
	)

	mkdir -p "$1/lib/ArgEnv/Clean System Files"
	cp "build/libraries/ArgEnv/ArgEnvC.o" "$1/lib/ArgEnv/Clean System Files/"

	for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README;
	do  cp build/libraries/ArgEnv/$f $1/lib/ArgEnv/$f
	done
}

build_dynamics() { #$1:target
	#Copy minimal Dynamics libraries
	mkdir -p "$1/lib/Dynamics/Clean System Files"
	cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.dcl $1/lib/Dynamics/
	cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.icl $1/lib/Dynamics/
	cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdDynamic.dcl $1/lib/Dynamics/
	cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdDynamicNoLinker.icl $1/lib/Dynamics/StdDynamic.icl
	cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/implementation/_SystemDynamic.dcl $1/lib/Dynamics/
	cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/implementation/_SystemDynamic.icl $1/lib/Dynamics/
}

build_directory() {
	mkdir -p build/Directory
	cp -r src/clean-libraries-master/Libraries/Directory/* build/Directory/
	cp src/clean-tools-trunk/htoclean/Clean.h "build/Directory/Clean System Files Unix/Clean.h"

	(cd "build/Directory/Clean System Files Unix"
		 cc -m32 -c -O cDirectory.c
	)
	mkdir -p "$1/lib/Directory/Clean System Files"
	cp build/Directory/*.[id]cl $1/lib/Directory/
	cp build/Directory/Clean\ System\ Files\ Unix/* $1/lib/Directory/Clean\ System\ Files/
}

build_clean_c_components () { # $1:target
	mkdir -p $1
	build_codegenerator $1
	build_runtimesystem_c $1
	build_clm $1
}

build_clean() { # $1:target
	mkdir -p $1

	#Copy all C components (they are built only once)
	cp -r target/clean-c-components/* $1/

	# Build all Clean components
	# Standard libraries
	build_stdenv $1
	# Rts and compiler
	build_runtimesystem_clean $1
	build_compiler $1 $2
}

#### Main build process ####

(cd dependencies/bootstrap-clean/lib/ArgEnv;
	cc -m32 -Wall -pedantic -O -c ArgEnvC.c
	mv ArgEnvC.o Clean\ System\ Files)

# Build components written in C (code generator, runtime system and clm)
build_clean_c_components target/clean-c-components

# First pass: build a clean system using the boot compiler
build_clean target/clean-intermediate -no-opt-link
# Standard libraries as dependencies for the second pass
build_dynamics target/clean-intermediate
build_directory target/clean-intermediate
build_argenv target/clean-intermediate

# Second pass: Build a minimal clean system using the clean system from the previous pass
rm -rf build
mkdir -p build/clean
cp -r target/clean-intermediate/* build/clean/
export BUILD_STDENV=yes

# TODO: Temporary: the new compiler generates new ABC code (with optimized
# instance calls), so we need to regenerate the ABC of StdEnv before trying to
# build a new Clean system. This needs to be fixed elsewhere.
build_stdenv target/new-stdenv
mv target/new-stdenv/lib/StdEnv/Clean\ System\ Files/*.abc build/clean/lib/StdEnv/Clean\ System\ Files
rm -rf target/new-stdenv

build_clean target/clean-base
build_compiler_itask target/clean-base
build_cpm target/clean-base
build_docs target/clean-base
