#!/bin/sh
mkdir -p dependencies
curl -L -o dependencies/clean.tgz https://ftp.cs.ru.nl/Clean/builds/linux-x64/clean-bundle-complete-linux-x64-latest.tgz
(cd dependencies
	mkdir -p clean
	tar -xzf clean.tgz -C clean --strip-components=1
)

# Android NDK
if [ ! -f "dependencies/ndk-r13b.zip" ]; then
	curl -L -o dependencies/ndk-r13b.zip https://dl.google.com/android/repository/android-ndk-r13b-linux-x86_64.zip
fi
(cd dependencies
	mkdir -p android-ndk
	unzip ndk-r13b.zip -d ./android-ndk
)
