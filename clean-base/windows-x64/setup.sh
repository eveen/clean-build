#!/bin/sh
mkdir -p dependencies
curl -L -o dependencies/bootstrap-clean.zip https://ftp.cs.ru.nl/Clean/Clean30/windows/Clean_3.0_64.zip
(cd dependencies
	unzip bootstrap-clean.zip
	mv "Clean 3.0" "bootstrap-clean"
)

# Setup the initial clean system
rm -rf build
mkdir -p build/clean
cp -r dependencies/bootstrap-clean/* build/clean/

# Temporary fix because kernel_library is too old; see
# https://gitlab.science.ru.nl/clean-compiler-and-rts/compiler/-/merge_requests/23
echo -e "\r\nSystemTimeToTzSpecificLocalTime" >> build/clean/Libraries/StdEnv/Clean\ System\ Files/kernel_library
