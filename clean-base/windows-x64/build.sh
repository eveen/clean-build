#!/bin/sh
set -e
export CUR_CLEAN_HOME=$CLEAN_HOME
export CUR_PATH=$PATH

PACKAGE=$1
OS=$2
ARCH=$3

#Visual studio compiler 
VSROOT="C:\Program Files (x86)\Microsoft Visual Studio 14.0"
#VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
#VSVARS="$VSROOT\VC\bin\vcvars32.bat"
#VSCC="$VSROOT\VC\bin\cl.exe"
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"

COMMAND=`cygpath --unix $COMSPEC`

#### Component build functions ####

build_codegenerator () { # $1:target 
    mkdir -p build
    cp -r src/code-generator-master build/codegenerator
    cd build/codegenerator
	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@nmake -f Makefile.windows64
EOBATCH
    cd ../..

    mkdir -p "$1/Tools/Clean System 64"
    cp build/codegenerator/cg.exe "$1/Tools/Clean System 64/CodeGenerator64.exe"
}
build_compiler_backend () { # $1: target
    mkdir -p build
    cp -r src/compiler-itask/backendC build/backend
    cd build/backend/CleanCompilerSources
    	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@nmake -f Makefile.windows64
EOBATCH
    cd ../../..

    mkdir -p "$1/Tools/Clean System 64"
    cp build/backend/backend.dll "$1/Tools/Clean System 64/backend.dll"
}

build_runtimesystem_c () { # $1:target
    mkdir -p build
    cp -r src/run-time-system-master build/runtimesystem

    cd build/runtimesystem
    	./build_windows_object_files_ml64.bat
    cd ../..

    mkdir -p $1/Libraries/StdEnv/Clean\ System\ Files

    #touch $1/Libraries/StdEnv/_startupProfile.dcl
    #touch $1/Libraries/StdEnv/_startupTrace.dcl

    for objectfile in _startup0.o _startup1.o _startup1Profile.o _startup1ProfileGraph.o _startup1ProfileGraphB.o _startup1Trace.o _startup2.o _startup3.o _startup4.o ; do
        cp build/runtimesystem/$objectfile "$1/Libraries/StdEnv/Clean System Files/"
    done
}
build_runtimesystem_clean () { # $1:target
    #Generate the 'clean part' of the runtime system
    mkdir -p $1/Libraries/StdEnv/Clean\ System\ Files


    #Copy abc and library files
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/_system.abc $1/Libraries/StdEnv/Clean\ System\ Files/_system.abc
    cp src/stdenv-master/Library\ Files\ Windows\ 64/*_library $1/Libraries/StdEnv/Clean\ System\ Files/

    #Use the new code generator
    target/clean-c-components/Tools/Clean\ System\ 64/CodeGenerator64.exe $1/Libraries/StdEnv/Clean\ System\ Files/_system
}

build_linker () { # $1:target $2: clean system
    mkdir -p build/linker
    cp -r src/clean-dynamic-system-trunk/* build/linker/

    #Patched project file for the linker
    cp $PACKAGE/$OS-$ARCH/txt/StaticLinker64.prj build/linker/StaticLinker64.prj

    cd build/linker
	LINKER_PRJ=`cygpath --windows --absolute "StaticLinker64.prj"`
	#Try cpm first, otherwise try the clean ide
	if test -f ../clean/cpm.exe; then
           ../clean/cpm.exe StaticLinker64.prj
	else
           ../clean/CleanIDE.exe --batch-build $LINKER_PRJ
	fi
    cd ../..

    mkdir -p "$1/Tools/Clean System 64"
    cp build/linker/StaticLinker.exe "$1/Tools/Clean System 64/StaticLinker.exe"
}

build_clm () { # $1:target
	mkdir -p build
	cp -r src/clm-master build/clm

	make -C build/clm -f Makefile.windows-x64 clm

	mkdir -p $1
	cp build/clm/clm.exe $1/clm.exe
}
build_cpm_intermediate () {
    #Copy the clean system for building
    mkdir -p build/cpm/clean-build
    mkdir -p build/cpm/clean-target

    cp -r build/clean/* build/cpm/clean-build/
    cp -r target/clean-intermediate/* build/cpm/clean-target/

    #Get batchbuild source code
    mkdir -p build/cpm/CleanIDE
    cp -r src/clean-ide-master/* build/cpm/CleanIDE/
    #Add dependencies
    cp -r src/clean-libraries-master/Libraries/StdLib/* build/cpm/CleanIDE/Util/

    #Patch project file
    cp $PACKAGE/$OS-$ARCH/txt/CpmWin.prj build/cpm/CleanIDE/CpmWin.prj

    #Create BatchBuild to bootstrap
	
    # Use 16M heap for batchbuild instead of 4M
    sed s/HeapSize:\\t4194304/HeapSize:\\t16777216/ < build/cpm/CleanIDE/BatchBuild.prj > build/cpm/CleanIDE/BatchBuild_.prj
    BATCHBUILD_PRJ=`cygpath --windows --absolute "build/cpm/CleanIDE/BatchBuild_.prj"`

    build/cpm/clean-build/CleanIDE.exe --batch-build $BATCHBUILD_PRJ

    #Create a fresh copy of the intermediate clean system + batch-build
    rm -rf build/cpm/clean-target/*
    cp -r target/clean-intermediate/* build/cpm/clean-target/
    cp build/cpm/CleanIDE/BatchBuild.exe build/cpm/clean-target/

    #Create a fresh copy of the IDE source for cpm
    rm -rf build/cpm/CleanIDE/*
    cp -r src/clean-ide-master/* build/cpm/CleanIDE/
    #Add dependencies
    cp -r src/clean-libraries-master/Libraries/StdLib/* build/cpm/CleanIDE/Util/
    #Patch project file
    cp $PACKAGE/$OS-$ARCH/txt/CpmWin.prj build/cpm/CleanIDE/CpmWin.prj

    # Patch IDEEnvs for batchbuild
    mkdir -p build/cpm/clean-target/Config
    cp $PACKAGE/$OS-$ARCH/txt/IDEEnvs build/cpm/clean-target/Config/IDEEnvs

    #Use batchbuild to create cpm
    CPM_PRJ=`cygpath --windows --absolute "build/cpm/CleanIDE/CpmWin.prj"`
    build/cpm/clean-target/BatchBuild.exe $CPM_PRJ

    #mkdir -p $1/Tools/Temp
    cp build/cpm/CleanIDE/cpm/cpm.exe target/clean-intermediate/cpm.exe

    mkdir -p target/clean-intermediate/Config
    cp $PACKAGE/$OS-$ARCH/txt/IDEEnvs target/clean-intermediate/Config/IDEEnvs
}

build_cpm_base() {
    echo "CPM BASE"

    #Copy the clean system for building
    mkdir -p build/cpm/clean-build

    cp -r target/clean-intermediate/* build/cpm/clean-build/

    #Get cpm source code
    mkdir -p build/cpm/CleanIDE
    cp -r src/clean-ide-master/* build/cpm/CleanIDE/
    #Add dependencies
    cp -r src/clean-libraries-master/Libraries/StdLib/* build/cpm/CleanIDE/Util/

    #Patch project file
    cp $PACKAGE/$OS-$ARCH/txt/CpmWin.prj build/cpm/CleanIDE/CpmWin.prj

    #Build
    cd build/cpm/CleanIDE
       ../clean-build/cpm.exe CpmWin.prj
    cd ../../..

    #mkdir -p $1/Tools/Temp
    cp build/cpm/CleanIDE/cpm/cpm.exe target/clean-base/cpm.exe

    mkdir -p target/clean-base/Config
    cp $PACKAGE/$OS-$ARCH/txt/IDEEnvs target/clean-base/Config/IDEEnvs
}

build_compiler() { # $1:target $2:clean system
    mkdir -p build/compiler
    cp -r src/compiler-itask/* build/compiler/

    cp build/compiler/backendC/CleanCompilerSources/backend.h build/compiler/backend/backend.h
    
    #TODO: Use an intermediately built htoclean

    BACKEND_H=`cygpath --windows --absolute "build/compiler/backend/backend.h"`
    build/clean/Tools/htoclean/htoclean.exe $BACKEND_H

    #TEMPORARY: Use patched project file, the one in the source does not work
    cp $PACKAGE/$OS-$ARCH/txt/CleanCompiler64.prj build/compiler/CleanCompiler64.prj

    cd build/compiler
	COCL_PRJ=`cygpath --windows --absolute "CleanCompiler64.prj"`

	#Try cpm first, otherwise try the clean ide
	if test -f ../clean/cpm.exe; then
           ../clean/cpm.exe CleanCompiler64.prj
	else
           ../clean/CleanIDE.exe --batch-build $COCL_PRJ
	fi
    cd ../..

    mkdir -p "$1/Tools/Clean System 64"

    #TEMPORARY HACK UNTIL WE HAVE A NEWER BOOTSTRAP VERSION
    if test -f build/compiler/CleanCompiler64.exe.exe; then
    	cp build/compiler/CleanCompiler64.exe.exe "$1/Tools/Clean System 64/CleanCompilerITasks64.exe"
    else
    	cp build/compiler/CleanCompiler64.exe "$1/Tools/Clean System 64/CleanCompilerITasks64.exe"
    fi
}

build_compiler_master() { # $1:target $2:clean system
    mkdir -p build/compiler-master
    cp -r src/compiler-master/* build/compiler-master/

    cp build/compiler-master/backendC/CleanCompilerSources/backend.h build/compiler-master/backend/backend.h

    #TODO: Use an intermediately built htoclean

    BACKEND_H=`cygpath --windows --absolute "build/compiler-master/backend/backend.h"`
    build/clean/Tools/htoclean/htoclean.exe $BACKEND_H

    #TEMPORARY: Use patched project file, the one in the source does not work
    cp $PACKAGE/$OS-$ARCH/txt/CleanCompiler64.prj build/compiler-master/CleanCompiler64.prj

    cd build/compiler-master
	COCL_PRJ=`cygpath --windows --absolute "CleanCompiler64.prj"`

	#Try cpm first, otherwise try the clean ide
	if test -f ../clean/cpm.exe; then
           ../clean/cpm.exe CleanCompiler64.prj
	else
           ../clean/CleanIDE.exe --batch-build $COCL_PRJ
	fi
    cd ../..

    mkdir -p "$1/Tools/Clean System 64"

    #TEMPORARY HACK UNTIL WE HAVE A NEWER BOOTSTRAP VERSION
    if test -f build/compiler-master/CleanCompiler64.exe.exe; then
    	cp build/compiler-master/CleanCompiler64.exe.exe "$1/Tools/Clean System 64/CleanCompiler64.exe"
    else
    	cp build/compiler-master/CleanCompiler64.exe "$1/Tools/Clean System 64/CleanCompiler64.exe"
    fi
}

build_docs () { #$1:target $2:clean system
    cp src/clean-ide-master/CleanLicenseConditions.txt $1/CleanLicenseConditions.txt
    #cp $PACKAGE/$OS-$ARCH/txt/README $1/README.md

    mkdir -p build/manual/clean-build
    cp -r build/clean/* build/manual/clean-build/

    cp -r src/language-report-master/write_clean_manual/* build/manual/
    (cd build/manual
        #Create generator
        clean-build/cpm.exe project write_clean_manual create
        clean-build/cpm.exe project write_clean_manual.prj set -h 20M
        clean-build/cpm.exe write_clean_manual.prj

		#Copy fonts
		cp ../../Fonts/LiberationSans-Regular.ttf .
		cp ../../Fonts/LiberationSans-Bold.ttf .
		cp ../../Fonts/LiberationSans-Italic.ttf .
		cp ../../Fonts/NimbusMonoPS-Regular.ttf .
		cp ../../Fonts/NimbusMonoPS-Bold.ttf .

        #Generate manual
		./write_clean_manual.exe
    )
    #Copy to target
    mkdir -p $1/Help
    cp build/manual/CleanLanguageReport.* $1/Help/
}

build_stdenv() { # $1:target $2: clean system
    mkdir -p $1/Libraries/StdEnv

    # Copy the platform independent files
    for a in _library _startup _system
    do cp src/stdenv-master/$a.dcl $1/Libraries/StdEnv/ ;
    done

    for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	  StdInt StdFile StdReal StdString \
	  StdFunc StdFunctions StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	  StdStrictLists StdTuple StdMaybe _SystemArray _SystemEnum _SystemEnumStrict \
	  _SystemStrictLists _SystemStrictMaybes StdGeneric;
    do cp src/stdenv-master/$a.[di]cl $1/Libraries/StdEnv/ ;
    done

    # Overwrite 64-bit specific files
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/_SystemStrictLists.icl $1/Libraries/StdEnv/_SystemStrictLists.icl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/_SystemStrictMaybes.icl $1/Libraries/StdEnv/_SystemStrictMaybes.icl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdInt.dcl $1/Libraries/StdEnv/StdInt.dcl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdReal.dcl $1/Libraries/StdEnv/StdReal.dcl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdReal.icl $1/Libraries/StdEnv/StdReal.icl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdString.dcl $1/Libraries/StdEnv/StdString.dcl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdString.icl $1/Libraries/StdEnv/StdString.icl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdFile.dcl $1/Libraries/StdEnv/StdFile.dcl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdFile.icl $1/Libraries/StdEnv/StdFile.icl

    STDENV_SYSTEM_MODULES="StdBool StdChar StdFile StdInt StdMisc StdReal StdString"
    for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
        "$1/Tools/Clean System 64/CleanCompiler64.exe" -ou -pm -pt -P "$1/Libraries/StdEnv/$stdenv_build_m" -dynamic;
    done
    for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
        "$1/Tools/Clean System 64/CleanCompiler64.exe" -ou -pm -pt -P "$1/Libraries/StdEnv/$stdenv_build_m"; 
    done
}

build_argenv() { # $1:target
    mkdir -p build/libraries/
    cp -r src/clean-libraries-master/Libraries/ArgEnvWindows build/libraries/ArgEnv
    #TODO: Should compile the c part of this library and not use object file from svn
    #(cd build/libraries/ArgEnv
    #    gcc -c ArgEnvC.c -o ArgEnvC.o
    #)

    mkdir -p "$1/Libraries/ArgEnv/Clean System Files"
    for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl README.txt;
    do  cp build/libraries/ArgEnv/$f $1/Libraries/ArgEnv/$f
    done
    cp build/libraries/ArgEnv/Clean\ System\ Files/* "$1/Libraries/ArgEnv/Clean System Files/"
    #cp "build/libraries/ArgEnv/ArgEnvC.o" "$1/Libraries/ArgEnv/Clean System Files/"
}

build_dynamics() { #$1:target
    #Copy minimal Dynamics libraries
    mkdir -p "$1/Libraries/Dynamics/Clean System Files"
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.dcl $1/Libraries/Dynamics/
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.icl $1/Libraries/Dynamics/
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdDynamic.dcl $1/Libraries/Dynamics/
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdDynamicNoLinker.icl $1/Libraries/Dynamics/StdDynamic.icl
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/implementation/_SystemDynamic.dcl $1/Libraries/Dynamics/
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/implementation/_SystemDynamic.icl $1/Libraries/Dynamics/
}
build_generics() { #$1:target
    #Copy generics library
    mkdir -p $1/Libraries/Generics
    cp src/clean-libraries-master/Libraries/GenLib/*.[id]cl $1/Libraries/Generics/
}
build_directory() { #$1:target
    mkdir -p build/Directory
    cp -r src/clean-libraries-master/Libraries/Directory/* build/Directory/
    cp src/clean-tools-trunk/htoclean/Clean.h "build/Directory/Clean System Files Windows/Clean.h"

    cd "build/Directory/Clean System Files Windows"
         gcc -c -O cDirectory.c
    cd ../../..

    mkdir -p "$1/Libraries/Directory/Clean System Files"
    cp build/Directory/*.[id]cl $1/Libraries/Directory/
    cp build/Directory/Clean\ System\ Files\ Windows/* $1/Libraries/Directory/Clean\ System\ Files/
}
build_stdlib() { #$1:target
    #Copy generics library
    mkdir -p $1/Libraries/StdLib
    cp src/clean-libraries-master/Libraries/StdLib/*.[id]cl $1/Libraries/StdLib/
}
build_objectio() { #$1:target
    mkdir -p build/ioobjects
    cp -r "src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/OS Windows/Windows_C_12" "build/objectio"

    #Compile c files
    for c in build/objectio/*.c; do
        vscc "$c"
    done

    #Copy .o files to target
    mkdir -p "$1/Libraries/ObjectIO/OS Windows/Clean System Files"
    cp build/objectio/*.obj "$1/Libraries/ObjectIO/OS Windows/Clean System Files/"
    # Copy clean files to target
    cp src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/*.[id]cl $1/Libraries/ObjectIO/
    cp src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/OS\ Windows/*.[id]cl $1/Libraries/ObjectIO/OS\ Windows/
    cp src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/OS\ Windows/Clean\ System\ Files/*_library $1/Libraries/ObjectIO/OS\ Windows/Clean\ System\ Files/
}

vscc() { # $1: C .c file path

	COMMAND=`cygpath --unix $COMSPEC`
	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.obj
	(cd "$vscc_dir"
	 cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@cl /nologo /c /GS- "$vscc_file" /Fo"$vscc_object_file"
EOBATCH
	)
}

build_clean_c_components () { # $1:target
    mkdir -p $1
    build_codegenerator $1
    build_compiler_backend $1
    build_runtimesystem_c $1
    build_clm $1
}

build_clean() { # $1:target $2: clean system
    mkdir -p $1

    #Copy all C components (they are built only once)
    cp -r target/clean-c-components/* $1/

    #Create a Temp directory for compiler output
    mkdir -p $1/Temp

    build_compiler $1 $2
    build_compiler_master $1 $2
    build_linker $1 $2

    # Build all Clean components
    # Standard libraries
    build_stdenv $1 $2
    # Rts and compiler
    build_runtimesystem_clean $1 $2
}

#### Main build process ####

# Build components written in C (code generator, runtime system and clm)
build_clean_c_components target/clean-c-components

# First pass: build a clean system using the bootstrap clean system
export PATH=`pwd`/build/clean/Tools:$CUR_PATH
export CLEAN_HOME=`cygpath --windows --absolute build/clean`

build_clean target/clean-intermediate build/clean
# Standard libraries as dependencies for the second pass
build_dynamics target/clean-intermediate build/clean
build_generics target/clean-intermediate build/clean
build_directory target/clean-intermediate build/clean
build_argenv target/clean-intermediate build/clean
build_stdlib target/clean-intermediate build/clean
build_objectio target/clean-intermediate build/clean

# TODO: Temporary: the new compiler generates new ABC code (with optimized
# instance calls), so we need to regenerate the ABC of StdEnv before trying to
# build a new Clean system. This needs to be fixed elsewhere.
rm build/clean/Libraries/StdEnv/Clean\ System\ Files/{Std,_System}*.abc

# Build cpm using the freshly built intermediate clean system
# and a little help from the bootstrap clean system
build_cpm_intermediate

# Preserve htoclean (we should build it instead)
mkdir -p target/clean-intermediate/Tools/htoclean
cp build/clean/Tools/htoclean/htoclean.exe target/clean-intermediate/Tools/htoclean/

# Second pass: Build a minimal clean system using the clean system from the previous pass
rm -rf build
mkdir -p build/clean
cp -r target/clean-intermediate/* build/clean/

build_clean target/clean-base build/clean
build_cpm_base
build_docs target/clean-base build/clean
