#!/bin/bash
mkdir -p dependencies
curl -L -o dependencies/bootstrap-clean.zip https://ftp.cs.ru.nl/Clean/Clean30/macosx/clean3.0.zip
(cd dependencies
	unzip bootstrap-clean.zip
	mv clean bootstrap-clean
	# Install
	(cd bootstrap-clean
		make
	)
)

# Setup the initial clean system
rm -rf build
mkdir -p build/clean
cp -r dependencies/bootstrap-clean/* build/clean/
