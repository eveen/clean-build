#!/bin/bash
set -e
SERVER=https://ftp.cs.ru.nl/Clean/builds

if [ $# -ne 3 ]; then
	echo "Usage: $0 <package name> <os name> <architecture name>"
	exit 1
fi

PACKAGE=$1
OS=$2
ARCH=$3
DEPS=$PACKAGE/$OS-$ARCH/dependencies.txt

# Create a fresh build directory with a clean system installed in build/clean
mkdir -p build/clean
mkdir -p test/clean
mkdir -p dependencies

function download_dependency() {
	if [ $OS == "windows" ]; then
		curl -L -o dependencies/$dep.zip $SERVER/$OS-$ARCH/$1-$OS-$ARCH-latest.zip
	else
		curl -L -o dependencies/$dep.tgz $SERVER/$OS-$ARCH/$1-$OS-$ARCH-latest.tgz
	fi
	(cd dependencies
		if [ $OS == "windows" ]; then
			unzip $1.zip
		else
			tar -xzf $1.tgz
		fi
	)
}

function build_dependency() {
	rm -rf dependencies/$1 target/$1

	#Setup dependencies
	./generic/setup.sh $1 $OS $ARCH
	#Fetch sources
	./generic/fetch.sh $1 $OS $ARCH

	#Build package
	./generic/build.sh $1 $OS $ARCH

	#Copy to destination 
	cp -r target/$1 dependencies/
}

function install_dependency() {
	cp -r dependencies/$1/* $2/

	find dependencies/$1 -name "*.env" | while read env; do
		if [ $OS == "windows" ]; then
			tail -n +3 $env >> $2/Config/IDEEnvs
		else
			tail -n +3 $env >> $2/etc/IDEEnvs
		fi
	done
}

#Main iteration over list of dependencies
if [ -e $DEPS ]; then
	tr -d '\r' < $DEPS | while read dep; do # (to be safe, fix windows line endings)
		#If the dependency is already available use it
		if [ ! -e dependencies/$dep ]; then
			# If a specific past date is set, build the dependency
			if [ -n "${CLEANDATE+set}" ]; then
				echo "Building dependency: $dep"
				build_dependency $dep
			# Check if we have already built the dependency
			elif [ -e target/$dep ]; then
				echo "Copying dependency: $dep"
				cp -r target/$dep dependencies/
			else
				echo "Downloading dependency: $dep"
				download_dependency $dep
			fi
		else
			echo "Skipping dependency: $dep"
		fi
		install_dependency $dep build/clean
		install_dependency $dep test/clean
	done
fi 

#If a package specific setup script is available, run that too
if [ -e $PACKAGE/$OS-$ARCH/setup.sh ]; then
	./$PACKAGE/$OS-$ARCH/setup.sh $PACKAGE $OS $ARCH
fi

