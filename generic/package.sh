#!/bin/sh
# Write a manifest file with all the used sources of the package
# before creating a zip archive with the package content
PACKAGE=$1
OS=$2
ARCH=$3

INFOFILE=target/$PACKAGE/Info/$PACKAGE-info.txt

#If a specific date is set, use that as the version name
if [ -n $CLEANDATE ]; then
	VERSION=`date +%Y%m%d`
else
	VERSION=$CLEANDATE
fi

# Write package defatils and build-version
mkdir -p `dirname $INFOFILE`
echo "$PACKAGE $OS $ARCH $VERSION" > $INFOFILE

# Write all versions of svn repositories
echo "[svn sources]" >> $INFOFILE
if [ -e $PACKAGE/$OS-$ARCH/svn-sources.txt ]; then
	tr -d '\r' < $PACKAGE/$OS-$ARCH/svn-sources.txt | while read repo branch; do

		branchbase=`basename $branch`
		if [ -e src/$repo-$branchbase ]; then
			#url=`svn info --no-newline --show-item url src/$repo-$branchbase`
			revision=`svn info --no-newline --show-item revision src/$repo-$branchbase`
			echo "$repo $branch $revision" >> $INFOFILE
		fi
	done
fi

# Write versions of git repositories
echo "[git sources]" >> $INFOFILE
if [ -e $PACKAGE/$OS-$ARCH/git-sources.txt ]; then
	tr -d '\r' < $PACKAGE/$OS-$ARCH/git-sources.txt | while read group repo branch; do

		if [ -e src/$repo-$branch ]; then

			cd src/$repo-$branch
			commit=`git show --format=%H -s`
			cd ../..

			echo "$group $repo $branch $commit" >> $INFOFILE
		fi
	done
fi

# Create zip or tgz archive
(cd target
	if [ $OS = "windows" ]; then
		zip -r $PACKAGE-$OS-$ARCH-$VERSION.zip $PACKAGE
	else
		tar -czf $PACKAGE-$OS-$ARCH-$VERSION.tgz $PACKAGE
	fi
)

