#!/bin/bash
SVN_BASEURL="https://svn.cs.ru.nl/repos"
GIT_BASEURL="https://gitlab.science.ru.nl"

if [ $# -ne 3 ]; then
	echo "Usage: $0 <package name> <os name> <architecture name>"
	exit 1
fi

PACKAGE=$1
OS=$2
ARCH=$3

mkdir -p src

if [ -e $PACKAGE/$OS-$ARCH/svn-sources.txt ]; then
	tr -d '\r' < $PACKAGE/$OS-$ARCH/svn-sources.txt | while read repo branch; do
		
		if [ ! -e src/$repo-`basename $branch` ]; then
			echo "Checking out svn repo: $repo / $branch"
			if [ -n "${CLEANDATE+set}" ]; then
				svn checkout -q -r "{$CLEANDATE}" $SVN_BASEURL/$repo/$branch src/$repo-`basename $branch`
			else 
				svn checkout -q $SVN_BASEURL/$repo/$branch src/$repo-`basename $branch`
			fi
		else
			echo "Skipping svn repo: $repo / $branch"
		fi
	done
fi

if [ -e $PACKAGE/$OS-$ARCH/git-sources.txt ]; then
	tr -d '\r' < $PACKAGE/$OS-$ARCH/git-sources.txt | while read group repo branch; do
		if [ ! -e src/$repo-$branch ]; then
			echo "Cloning git repo: $group / $repo / $branch"
			if [ -n "${CLEANDATE+set}" ]; then
				git clone -b $branch $GIT_BASEURL/$group/$repo.git src/$repo-$branch
				(cd src/$repo-$branch
					git checkout `git rev-list -n 1 --before="$CLEANDATE" $branch`
					git submodule update --init --recursive)
			else
				git clone --recursive -b $branch $GIT_BASEURL/$group/$repo.git src/$repo-$branch
			fi
		else
			echo "Skipping git repo: $group / $repo / $branch"
		fi
	done
fi
