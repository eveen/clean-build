#!/bin/bash

if [ $# -ne 3 ]; then
	echo "Usage: $0 <package name> <os name> <architecture name>"
	exit 1
fi

PACKAGE=$1
OS=$2
ARCH=$3

#Just run the test script if it exists
if [ -e $PACKAGE/$OS-$ARCH/test.sh ]; then
	./$PACKAGE/$OS-$ARCH/test.sh $PACKAGE $OS $ARCH
fi
