#!/bin/sh
set -e

cp -r src/clean-completion-master build/clean-completion
make -C build/clean-completion
mkdir -p target
tar xzvf build/clean-completion/clean-completion.tar.gz -C target
