#!/bin/sh

PACKAGE=$1
OS=$2
ARCH=$3

TARGET=target/clean-lib-objectio

#Visual studio compiler 
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
COMMAND=`cygpath --unix $COMSPEC`

vscc() { # $1: C .c file path

	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.obj
	(cd "$vscc_dir"
	 cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@cl /nologo /GS- /c "$vscc_file" /Fo"$vscc_object_file"
EOBATCH
	)
}


mkdir -p build/objectio
cp -r src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/OS\ Windows/Windows_C_12/* build/objectio/

#Copy everything !ONLY BECAUSE THIS IS THE ONLY WAY TO BUILD THE CleanIDE...
mkdir -p "$TARGET/Libraries/ObjectIO"
cp -r src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/* $TARGET/Libraries/ObjectIO/

#Compile c files
for c in build/objectio/*.c; do
   echo $c
   vscc "$c"
done

#Copy .obj files to target
mkdir -p "$TARGET/Libraries/ObjectIO/OS Windows/Clean System Files"
cp build/objectio/*.obj "$TARGET/Libraries/ObjectIO/OS Windows/Clean System Files/"

# Copy clean files to target
#cp src/clean-libraries/Libraries/ObjectIO/ObjectIO/*.[id]cl $TARGET/Libraries/ObjectIO/
#cp src/clean-libraries/Libraries/ObjectIO/ObjectIO/OS\ Windows/*.[id]cl $TARGET/Libraries/ObjectIO/OS\ Windows/
#cp src/clean-libraries/Libraries/ObjectIO/ObjectIO/OS\ Windows/Clean\ System\ Files/*_library $TARGET/Libraries/ObjectIO/OS\ Windows/Clean\ System\ Files/

# Copy examples
mkdir -p $TARGET/Examples/ObjectIO
cp -r src/clean-libraries-master/Examples/ObjectIO/ObjectIO\ Examples/* $TARGET/Examples/ObjectIO/

# Copy environment
mkdir -p $TARGET/Config
cp $PACKAGE/$OS-$ARCH/txt/ObjectIO.env $TARGET/Config/
