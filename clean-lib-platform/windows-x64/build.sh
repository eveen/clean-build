#!/bin/sh
mkdir -p target/clean-lib-platform

# Add libraries
mkdir -p target/clean-lib-platform/Libraries/Platform

# Compile c dependencies
make -C src/clean-platform-master/src/cdeps install

cp -r src/clean-platform-master/src/libraries/OS-Independent/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Windows/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Windows-64/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform-master/src/libraries/Platform-x86/* target/clean-lib-platform/Libraries/Platform/
