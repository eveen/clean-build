#!/bin/sh
. ./config.sh # Path to cross compilers

mkdir -p target/clean-lib-platform

# Build c library

(cd src/clean-platform/src/libraries/OS-Independent/Text/Unicode ; make CC=$CROSS_CC CFLAGS="$CROSS_CFLAGS")
mkdir -p src/clean-platform/src/libraries/OS-Android-arm32/Clean\ System\ Files
mv src/clean-platform/src/libraries/OS-Independent/Text/Unicode/*.o src/clean-platform/src/libraries/OS-Android-arm32/Clean\ System\ Files/

# Add libraries
mkdir -p target/clean-lib-platform/lib/Platform

cp -r src/clean-platform/src/libraries/OS-Independent/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Posix/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Linux/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Android/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Android-32/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Android-arm32/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/Platform-ARM/* target/clean-lib-platform/lib/Platform/
