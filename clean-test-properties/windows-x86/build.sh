#!/bin/sh
set -e
mkdir -p target/clean-test-properties

export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME:$CLEAN_HOME\\Tools:$PATH

cp -r src/clean-test-properties-master/src build/clean-test-properties

make -j -C build/clean-test-properties/compiler/backendC/CleanCompilerSources -f Makefile.windows_mingw

(cd build/clean-test-properties
	mv testproperties_win.prj.default testproperties.prj
	cpm.exe project testproperties.prj build
)

cp build/clean-test-properties/testproperties.exe target/clean-test-properties/testproperties.exe
