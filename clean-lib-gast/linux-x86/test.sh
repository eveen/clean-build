set -e
# Set up
mkdir -p "test/clean"
mkdir -p "test/tests"
cp -r build/clean/* test/clean/
cp -r target/clean-lib-gast/* test/clean/
cp -r src/gast-master/Tests/* test/tests
tail -n +3 test/clean/etc/Gast.env >> test/clean/etc/IDEEnvs

# Compile and run
(cd "test"
	export CLEAN_HOME=`pwd`/clean
	export PATH=$CLEAN_HOME/bin:$PATH
	echo $CLEAN_HOME
	
	# Compile unit tests
	cp tests/test0.prj.default tests/test0.prj
	cpm tests/test0.prj
	# Execute unit tests
	./tests/test0.exe
)
