set -e
# Set up
mkdir -p "test/clean"
mkdir -p "test/tests"
cp -r build/clean/* test/clean/
cp -r target/clean-lib-gast/* test/clean/
cp -r src/gast-master/Tests/* test/tests
tail -n +3 test/clean/Config/Gast.env >> test/clean/Config/IDEEnvs

# Compile and run
export CLEAN_HOME=`cygpath --windows --absolute test/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH
(cd "test"
 # Compile unit tests
 (cd "tests"
 sed s/StdEnv/Gast/ < test0.prj.default > test0.prj
 ../clean/cpm.exe test0.prj

 # Execute unit tests
 ./test0.exe
 )
)
