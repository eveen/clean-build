#!/bin/sh
mkdir -p target/clean-lib-gast

# Add libraries
mkdir -p target/clean-lib-gast/lib/Gast
cp -r src/gast-master/Libraries/* target/clean-lib-gast/lib/Gast/

# Add environments
mkdir -p target/clean-lib-gast/etc
cp src/gast-master/Config/macos-x64/Gast.env target/clean-lib-gast/etc/Gast.env
