#!/bin/sh
mkdir -p target/clean-lib-dynamics/lib/Dynamics

cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.dcl target/clean-lib-dynamics/lib/Dynamics
cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.icl target/clean-lib-dynamics/lib/Dynamics
cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdDynamic.dcl target/clean-lib-dynamics/lib/Dynamics
cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdDynamicNoLinker.icl target/clean-lib-dynamics/lib/Dynamics/StdDynamic.icl
cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/implementation/_SystemDynamic.dcl target/clean-lib-dynamics/lib/Dynamics
cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/implementation/_SystemDynamic.icl target/clean-lib-dynamics/lib/Dynamics
