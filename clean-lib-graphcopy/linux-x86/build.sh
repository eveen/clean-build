#!/bin/sh
mkdir -p target/clean-lib-graphcopy

# Add clean libraries
mkdir -p target/clean-lib-graphcopy/lib/GraphCopy
cp -r src/clean-libraries-master/Libraries/graph_copy/*.[di]cl target/clean-lib-graphcopy/lib/GraphCopy/
cp -r src/clean-libraries-master/Libraries/graph_copy/linux/*.[di]cl target/clean-lib-graphcopy/lib/GraphCopy/

# Build c library
mkdir -p build/clean-graph-copy
cp -r src/clean-libraries-master/Libraries/graph_copy/* build/clean-graph-copy
rm -rf build/clean-graph-copy/Clean\ System\ Files
mkdir build/clean-graph-copy/Clean\ System\ Files
(cd build/clean-graph-copy
    make -e -f Makefile.linux
)
cp -r build/clean-graph-copy/Clean\ System\ Files target/clean-lib-graphcopy/lib/GraphCopy/
