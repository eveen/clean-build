#!/bin/sh

#Visual studio compiler
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
COMMAND=`cygpath --unix $COMSPEC`

mkdir -p target/clean-lib-graphcopy

# Add clean libraries
mkdir -p target/clean-lib-graphcopy/Libraries/GraphCopy/Clean\ System\ Files/
cp -r src/clean-libraries-master/Libraries/graph_copy/*.[id]cl target/clean-lib-graphcopy/Libraries/GraphCopy/
cp -r src/clean-libraries-master/Libraries/graph_copy/win/*.[id]cl target/clean-lib-graphcopy/Libraries/GraphCopy/

# Build c library
mkdir -p build/clean-graph-copy
cp -r src/clean-libraries-master/Libraries/graph_copy/* build/clean-graph-copy
rm -rf build/clean-graph-copy/Clean\ System\ Files
mkdir build/clean-graph-copy/Clean\ System\ Files
(cd build/clean-graph-copy
	 cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@nmake -f Makefile64 all
EOBATCH
)
cp -r build/clean-graph-copy/Clean\ System\ Files target/clean-lib-graphcopy/Libraries/GraphCopy/
