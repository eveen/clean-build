#!/usr/bin/env python2

# This library indexes the packages in this repository in a convenient data
# structure

import os

PACKAGE_ROOT = os.path.join(os.path.dirname(__file__), '..')

EXCLUDED = [
    "Documentation",
    "Fonts",
    "Tools",
    "README.md",
    "generic",
    "build",
    "dependencies",
    "src",
    "target",
    "test",
    ]


def indexPackages():
    """Index all packages"""
    packages = []
    for package_name in os.listdir(PACKAGE_ROOT):
        # Exclude non-package entries
        if package_name[0] == '.'\
                or package_name in EXCLUDED:
            continue

        packages.append(indexPackage(package_name))

    return packages


def indexPackage(package_name):
    """Check for which target platforms the package exists"""
    package = {}
    package["name"] = package_name
    package["targets"] = []
    for target_name in os.listdir(os.path.join(PACKAGE_ROOT, package_name)):
        if target_name[0] == '.':
            continue

        package["targets"].append(indexTarget(package_name, target_name))

    package["dependencies"] = aggregateDependencies(package["targets"])

    package["issues"] = []
    package["issues"].extend(
            checkMultipleSourceRepos(package["dependencies"]["source"]))
    package["issues"].extend(checkHasTests(package))
    return package


def indexTarget(package_name, target):
    source_dependencies = indexSourceDependencies(package_name, target)
    (package_dependencies, other_dependencies) = indexBinaryDependencies(
            package_name, target)

    return {"name": target,
            "dependencies": {
                "source": source_dependencies,
                "package": package_dependencies,
                "other": other_dependencies},
            "has_setup": os.path.isfile(os.path.join(
                PACKAGE_ROOT, package_name, target, "setup.sh")),
            "has_fetch": os.path.isfile(os.path.join(
                PACKAGE_ROOT, package_name, target, "fetch.sh")),
            "has_build": os.path.isfile(os.path.join(
                PACKAGE_ROOT, package_name, target, "build.sh")),
            "has_test": os.path.isfile(os.path.join(
                PACKAGE_ROOT, package_name, target, "test.sh")),
            "has_package": os.path.isfile(os.path.join(
                PACKAGE_ROOT, package_name, target, "package.sh"))
            }


def indexSourceDependencies(package_name, target):
    dependencies = []
    # For each target check which source repositories it uses
    fetchsh = os.path.join(PACKAGE_ROOT, package_name, target, "fetch.sh")
    if os.path.isfile(fetchsh):
        # The scripts often contain shell variables which complicates indexing
        # a bit
        sh_vars = {}
        for line in open(fetchsh, "r"):
            items = sh_vars.items()
            items.sort(reverse=True)
            # Substitute shell variables
            for (key, value) in items:
                line = line.replace("$"+key, value)
            # Check new assignments
            if line.find("=") >= 0:
                parts = line.split("=")
                sh_vars[parts[0].strip()] = removeQuotes_(parts[1].strip())
            # Check svn exports
            elif line.find("svn export") == 0:
                urls = filter(
                    lambda x: x.find("https://") >= 0, line.split(" "))
                url = removeQuotes_(urls[0].strip())
                # Find repo name and path
                parts = (url[url.find("repos") + 6:]).split("/")
                name = parts[0]
                path = "/".join(parts[1:])

                repository = {"type": "svn", "name": name,
                              "path": path, "url": url}
                dependencies.append(repository)
            # Check git clones
            elif line.find("git clone") == 0:
                urls = filter(
                    lambda x: x.find("https://") >= 0, line.split(" "))
                url = removeQuotes_(urls[0].strip())
                name = url[url.find("clean-and-itasks") + 17:]
                repository = {"type": "git", "name": name, "url": url}
                dependencies.append(repository)

    return dependencies


def indexBinaryDependencies(package_name, target):
    """For each target check which packages required at build time"""
    package_dependencies = []
    other_dependencies = []

    setupsh = os.path.join(PACKAGE_ROOT, package_name, target, "setup.sh")
    if os.path.isfile(setupsh):
        sh_vars = {}
        for line in open(setupsh, "r"):
            items = sh_vars.items()
            items.sort(reverse=True)
            # Substitute shell variables
            for (key, value) in items:
                line = line.replace("$"+key, value)
            # Check new assignments
            if line.find("=") >= 0:
                parts = line.split("=")
                sh_vars[parts[0].strip()] = removeQuotes_(parts[1].strip())
            # Check the download lines
            if line.find("curl") == 0:
                # Find downloads of clean packages (can be recognized by the
                # date command :) )
                if line.find("date") >= 0:
                    start = line.find("date +") + 6
                    end = line.find(target + "-%Y%m%d") - 1

                    package_dependencies.append(line[start:end])
                # Include other downloads as a 'other' dependencies
                elif line.find("http://") >= 0 or line.find("ftp://") >= 0:
                    urls = filter(lambda x: x.find("http://") >= 0
                                  or x.find("ftp://") >= 0, line.split(" "))
                    other_dependencies.append(urls[0])

    return (package_dependencies, other_dependencies)


def aggregateDependencies(package_targets):
    package = []
    source = []
    other = []
    for target in package_targets:
        package.extend(target["dependencies"]["package"])
        source.extend(target["dependencies"]["source"])
        other.extend(target["dependencies"]["other"])

    return {"package": removeDuplicates_(package),
            "source": removeDuplicates_(source),
            "other": removeDuplicates_(other)}


def checkMultipleSourceRepos(dependencies):
    if len(dependencies) > 1:
        return ["This uses %i source repositories. "
                "There should be only one per package." % (len(dependencies))]
    else:
        return []


def checkInconsistentDependencies(dependencies, targets):
    return []


def checkHasTests(package):
    issues = []

    for target in package["targets"]:
        if not target["has_test"]:
            issues.append("This package is not tested on platform '%s'" %
                          target["name"])

    return issues


def removeDuplicates_(l):
    return [x for i, x in enumerate(l) if i == l.index(x)]


def removeQuotes_(s):
    if len(s) >= 2 and (s[0] == '"') and (s[-1] == '"'):
        return s[1:-1]
    else:
        return s
