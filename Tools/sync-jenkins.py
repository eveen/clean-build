#!/usr/bin/env python

# This script creates Jenkins build jobs for all packages and synchronizes
# them with a build server using the Jenkins CLI tool

import os
import subprocess
import copy
import xml.etree.ElementTree as ET
import ConfigParser
import cleanbuild

# Read the jenkins credentials from a local file with the
config = ConfigParser.ConfigParser()
config.read('jenkins.cfg')

JENKINS_URL = config.get("Jenkins", "url")
JENKINS_USER = config.get("Jenkins", "user")
JENKINS_TOKEN = config.get("Jenkins", "token")

JOB_NOTIFY_EMAIL = config.get("Jenkins", "notify")
JOB_PUBLISH_SERVER = config.get("Jenkins", "publishserver")
JOB_PUBLISH_ROOTDIR = config.get("Jenkins", "publishdir")

# Determine the package root directory from the script filename
PACKAGE_ROOT = os.path.join(os.path.dirname(__file__), '..')

# Jenkins CLI access
def getJenkinsCLI():
    if not os.path.isfile("cli.jar"):
        subprocess.call("curl -o cli.jar %sjnlpJars/jenkins-cli.jar" %
                        JENKINS_URL, shell=True)


def listJenkinsJobs():
    output = subprocess.check_output(jenkinsCommand("list-jobs"), shell=True)
    return output.strip().split("\n")


def getJenkinsJob(name):
    output = subprocess.check_output(
        jenkinsCommand("get-job %s" % name), shell=True)
    if output.startswith("ERROR: No such job"):
        return None
    else:
        return output


def updateJenkinsJob(name, xml):
    proc = subprocess.Popen(jenkinsCommand(
        "update-job %s" % name), stdin=subprocess.PIPE, shell=True)
    proc.stdin.write(xml)
    proc.stdin.close()


def createJenkinsJob(name, xml):
    proc = subprocess.Popen(jenkinsCommand(
        "create-job %s" % name), stdin=subprocess.PIPE, shell=True)
    proc.stdin.write(xml)
    proc.stdin.close()


def jenkinsCommand(command):
    return "java -jar cli.jar -http -s %s -auth %s:%s %s" %\
        (JENKINS_URL, JENKINS_USER, JENKINS_TOKEN, command)

# Generate a jenkins job definition for a package
def generateJenkinsJob(package, target):

    jobname = "%s-%s" % (package["name"], target["name"])
    target_parts = target["name"].split("-")
    osname = target_parts[0]
    archname = target_parts[-1]

    # Load the template xml file
    jobtemplate = ET.parse("jenkins-job.tpl")
    transfertemplate = ET.parse("jenkins-transfer.tpl")

    project = jobtemplate.getroot()
    project.find(
        "description").text = "Generated job for Clean package %s" % jobname
    # Set which package in the repository should be checked out
    # project.find(".//hudson.plugins.git.extensions.impl.SparseCheckoutPath/path").text = "%s/%s" % (package["name"],target["name"])

    # Set on what build node the job can be built
    project.find(".//assignedNode").text = target["name"]

    # Create the build script
	# We need to generate slightly different scripts for
	# the different os targets
    if target["name"].startswith("windows"):

        if target["name"].endswith("x64"):
            msyspath = "c:\\msys64\\mingw64\\bin;c:\\msys64\\usr\\bin"
            msyssh = "c:\\msys64\\usr\\bin\\bash.exe"
        else:
            msyspath = "c:\\msys32\\mingw32\\bin;c:\\msys32\\usr\\bin"
            msyssh = "c:\\msys32\\usr\\bin\\bash.exe"

        for step in ["setup","fetch","build","test","package"]:
            script = [("set PATH=%s;" % msyspath) + "%PATH%"
                     ,("%s generic/%s.sh %s %s %s" % (msyssh,step,package["name"],osname,archname))]

            task = ET.SubElement(project.find("builders"),"hudson.tasks.BatchFile")
            command = ET.SubElement(task, "command")
            command.text = "\n".join(script)
    else:
        script = []
        script.append("bash generic/setup.sh %s %s %s" % (package["name"],osname,archname))
        script.append("bash generic/fetch.sh %s %s %s" % (package["name"],osname,archname))
        script.append("bash generic/build.sh %s %s %s" % (package["name"],osname,archname))
        script.append("bash generic/test.sh %s %s %s" % (package["name"],osname,archname))
        script.append("bash generic/package.sh %s %s %s" % (package["name"],osname,archname))

        task = ET.SubElement(project.find("builders"), "hudson.tasks.Shell")
        command = ET.SubElement(task, "command")
        command.text = "\n".join(script)

    # Set notification e-mail
    project.find(".//hudson.tasks.Mailer/recipients").text = JOB_NOTIFY_EMAIL

    # Add transfer commands (if something has been packaged)
    project.find(
       ".//jenkins.plugins.publish__over__ssh.BapSshPublisher/configName").text = JOB_PUBLISH_SERVER

    if target["name"].startswith("windows"):
        archivetype = "zip"
    else:
        archivetype = "tgz"

    transfers =\
        [{"execCommand": "rm -f %s/%s/%s-%s*.%s" %
            (JOB_PUBLISH_ROOTDIR, target["name"], package["name"],
             target["name"], archivetype)},
         {"sourceFiles": "target/%s-%s*.%s" %
            (package["name"], target["name"], archivetype),
          "remoteDirectory": target["name"],
          "removePrefix": "target/",
          "execCommand": "chmod 644 %s/%s/%s-%s*.%s" %
            (JOB_PUBLISH_ROOTDIR, target["name"], package["name"],
             target["name"], archivetype)},
         {"execCommand":
          "ln -s `date +%s-%s-%%Y%%m%%d.%s` %s/%s/%s-%s-latest.%s" %
            (package["name"], target["name"], archivetype,
             JOB_PUBLISH_ROOTDIR, target["name"], package["name"],
             target["name"], archivetype)}]
    for transfer in transfers:
        transfernode = copy.deepcopy(transfertemplate.getroot())
        for (key, value) in transfer.items():
            transfernode.find(key).text = value

        project.find(
            ".//jenkins.plugins.publish__over__ssh.BapSshPublisher/transfers").append(transfernode)

    return ET.tostring(project, encoding="utf-8", method="xml")

# Create a jenkins job definition as xml for a package
def syncJenkinsServer(packages):
    # Download the jenkins cli tool if necessary
    getJenkinsCLI()

    # Get the list of current jenkins jobs
    jobs = listJenkinsJobs()

    # Sync all packages
    for package in packages:
        for target in package["targets"]:
            jobname = "%s-%s" % (package["name"], target["name"])

            xml = "<?xml version='1.0' encoding='UTF-8'?>" + \
                generateJenkinsJob(package, target)
            if jobname in jobs:
                print("Updating '%s'..." % jobname)
                updateJenkinsJob(jobname, xml)
            else:
                print("Creating '%s'..." % jobname)
                createJenkinsJob(jobname, xml)


if __name__ == "__main__":
    syncJenkinsServer(cleanbuild.indexPackages())
