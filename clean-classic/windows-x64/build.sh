#!/bin/sh

#set -vx # shell debug options
set -e

# strategy:
# get all sources from svn
# copy and build from non-clean sources
# copy build from clean sources (with distribution)
# build distribution step 1
# test
# copy build from clean sources (with new build)
# test

# TODO
#   ... reduce number of C compilers
#   ... build *everything* from svn 
#   ... also for other platforms, mac should be easiest
#   ... test bootstrap

BUILDSCRIPT=`cygpath --unix --absolute "$0"`
BUILDDIR=`dirname "$BUILDSCRIPT"`

#
# Configure section
#

# Where to build
ROOT="$BUILDDIR"

# dos command shell
COMMAND=`cygpath --unix $COMSPEC`

SVN=svn

# Clean System
CLEANROOT="`pwd`/Clean_3.0_64"

#VSSETENV="C:\Program Files\Microsoft SDK\SetEnv.bat"
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"

STDENV_SYSTEM_MODULES="StdBool StdChar StdFile StdInt StdMisc StdReal StdString"


log ()
	# $1: message
{
	echo "$1"
}

fatal ()
	# $1: message
{
	log "$1"
	exit 1
}

checkreturncodeoptionallyfail ()
	# $1: where
	# $2: error message
	# $3: return code
	# $4: 0: don't fail, otherwise: fail
{
	if test $3 -ne 0; then
		if test $4 -eq 0; then
			log "$1: warning ($3) $2"
		else
			fatal "$1: error ($3) $2"
		fi
	fi
}

checkreturncode ()
	# $1: where
	# $2: error message
	# $3: return code
{
	checkreturncodeoptionallyfail "$1" "$2" $3 1
}

checkcleanreturncode ()
	# $1: clean system dir
	# $2: where
	# $3: error message
	# $4: return code
{
	if test "$1" = "$CLEANROOT" ; then
		checkcleanreturncode_fail=0
	else
		checkcleanreturncode_fail=1
	fi

	checkreturncodeoptionallyfail "$2" "$3" $4 $checkcleanreturncode_fail
}

svn_get ()
	# $1 $2 $3
{
	w1=`cygpath --absolute --windows "$1"`
	log "svn '$2' '$3'"
	$SVN export -q "https://svn.cs.ru.nl/repos/$2" "$w1/$3"
}

git_get ()
	# $1 $2 $3
{
	w1=`cygpath --absolute --windows "$1"`
	log "git '$2' '$3'"
	"/c/Program Files/Git/cmd/git.exe" clone --depth 1 "https://gitlab.science.ru.nl/$2" "$w1/$3"
}

makedir ()
	# $1: directory path
{
	if test -e "$1"; then
		fatal "makedir: directory '$1' already exists"
	fi

	mkdir -p "$1"
	checkreturncode "makedir" "creating '$1'" $?
}

move ()
	# $1: source path
	# $2: destination path
{
	if test ! -e "$1"; then
		fatal "move source '$1' does not exist"
	fi
	if test -e "$2"; then
		fatal "move destination '$2' already exists"
	fi

	mv "$1" "$2"
	checkreturncode "move" "moving '$1' to '$2'" $?
}

remove ()
	# $1: directory or file path
{
	if test ! -e "$1"; then
		fatal "remove '$1' does not exist"
	fi
	rm -r "$1"
	checkreturncode "remove" "removing '$1'" $?
}

duplicate ()
	# $1: source path
	# $2: destination path
{
	log "duplicating '$1' -> '$2'"

	if test ! -e "$1"; then
		fatal "duplicate: source '$1' does not exist"
	fi
	if test -e "$2"; then
		fatal "duplicate: destination '$2' already exists"
	fi

	cp -r "$1" "$2"
	checkreturncode "duplicate" "copying '$1' to '$2'" $?
}

vscc ()
	# $1: C .c file path
{
	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.obj
	(cd "$vscc_dir"
	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@cl /nologo /GS- /c /O "$vscc_file" /Fo"$vscc_object_file"
@if %errorlevel% neq 0 exit /b %errorlevel%
EOBATCH
	checkreturncode "vscc" "compiling '$1'" $?
	)
}

vscc_directx ()
	# $1: C .c file path
{
	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.obj
	(cd "$vscc_dir"
	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@cl /nologo /GS- /c /O2 /I"C:\Program Files (x86)\Microsoft DirectX SDK (October 2006)\Include" "$vscc_file" /Fo"$vscc_object_file"
@if %errorlevel% neq 0 exit /b %errorlevel%
EOBATCH
	checkreturncode "vscc" "compiling '$1'" $?
	)
}

cleancocl ()
	# $1: clean system dir
	# $2: clean icl module path
	# $3: dynamic option
{
	TMP=/tmp # +++
	cleancocl_error=`cygpath --windows "$TMP/cocl.error"`
	cleancocl_out=`cygpath --windows "$TMP/cocl.out"`

	cleancocl_moduledir=`dirname "$2"`
#	cleancocl_moduledir=`cygpath --windows --absolute "$cleancocl_moduledir"`
	cleancocl_module=`basename "$2"`
	cleancocl_sytemdir=`cygpath --windows --absolute "$1"`

	if test "$3"x = "-dynamic"x; then
		cleancocl_paths="."
		cleancocl_paths="$cleancocl_paths"\;"$cleancocl_sytemdir"\\Libraries\\Dynamics
		cleancocl_dynamics="-dynamics"
	else
		cleancocl_paths="."
		cleancocl_dynamics=
	fi
	

	(cd "$cleancocl_moduledir"

	echo "$1/Tools/Clean System 64"/CleanCompiler64 \
		-P "$cleancocl_paths" \
		-RE "$cleancocl_error" -RO "$cleancocl_out" \
		-ou -pm -pt -wmt $cleancocl_dynamics \
		"$cleancocl_module" 
	"$1/Tools/Clean System 64"/CleanCompiler64 \
		-P "$cleancocl_paths" \
		-RE "$cleancocl_error" -RO "$cleancocl_out" \
		-ou -pm -pt -wmt $cleancocl_dynamics \
		"$cleancocl_module" 
		# -RO: redirect error/out
		# -ou: reuse unique nodes / -pm: memory profile / -pt: time profile
	)

	checkcleanreturncode "$1" "cleancocl" "compiling '$2'" $?
}

newcleancocl ()
	# $1: clean system dir
	# $2: clean icl module path
	# $3: compiler dir
	# $4: dynamic option
{
	TMP=/tmp # +++
	cleancocl_error=`cygpath --windows "$TMP/cocl.error"`
	cleancocl_out=`cygpath --windows "$TMP/cocl.out"`

	cleancocl_moduledir=`dirname "$2"`
	cleancocl_module=`basename "$2"`
	cleancocl_sytemdir=`cygpath --windows --absolute "$1"`

	if test "$4"x = "-dynamic"x; then
		cleancocl_paths="."
		cleancocl_paths="$cleancocl_paths"\;"$cleancocl_sytemdir"\\Libraries\\Dynamics
		cleancocl_dynamics="-dynamics"
	else
		cleancocl_paths="."
		cleancocl_dynamics=
	fi
	

	(cd "$cleancocl_moduledir"

	echo "$3"/CleanCompiler64 \
		-P "$cleancocl_paths" \
		-RE "$cleancocl_error" -RO "$cleancocl_out" \
		-ou -pm -pt -wmt $cleancocl_dynamics \
		"$cleancocl_module"
	"$3"/CleanCompiler64 \
		-P "$cleancocl_paths" \
		-RE "$cleancocl_error" -RO "$cleancocl_out" \
		-ou -pm -pt -wmt $cleancocl_dynamics \
		"$cleancocl_module" 
		# -RO: redirect error/out
		# -ou: reuse unique nodes / -pm: memory profile / -pt: time profile
	)

	checkcleanreturncode "$1" "cleancocl" "compiling '$2'" $?
}

cleanbuild ()
	# $1: clean system dir
	# $2: clean project file path
{
	cleanbuild_project=`cygpath --windows --absolute "$2"`

	log "compiling $cleanbuild_project"

	"$1/CleanIDE" --batch-build "$cleanbuild_project"

	checkcleanreturncode "$1" "cleanbuild" "building '$2'" $?
}

htoclean ()
	# $1: htoclean path
	# $2: C .h file path
{
	"$1" "$2"

	checkreturncode "htoclean" "converting '$2'" $?
}

buildallcleanprojectfiles ()
	# $1: clean system dir
	# $2: directory with projects to build
{
    for prj in "$2"/*.prj; do
        if test -f "$prj"; then
            cleanbuild "$1" "$prj"
        fi
    done

    # recurse in dirs
    for dir in "$2"/*; do
        if test -d "$dir"; then
			buildallcleanprojectfiles "$1" "$dir"
        fi
    done
}

#CLEAN_LIBRARIES="ArgEnvWindows Directory ExtendedArith GEC Gast Hilde\
#					MersenneTwister Parsers StdEnv StdLib WrapDebug ExceptionsWindows"
CLEAN_LIBRARIES="Directory MersenneTwister Parsers StdLib WrapDebug TCPIP"

xmakedir ()
	# $1: directory path
{
	mkdir -p "$1"
	checkreturncode "makedir" "creating '$1'" $?
}

cvscleansystem ()
	# $1: destination directory
{
	xmakedir "$1"

	git_get "$1" clean-compiler-and-rts/stdenv.git libraries/StdEnv

	w1=`cygpath --absolute --windows "$1"`
	rm -rf "$w1/libraries/StdEnv/.git"

	git_get "$1" clean-and-itasks/clean-libraries.git clean-libraries-git
	xmakedir "$1/libraries"
	xmakedir "$1/CleanExamples"
	for cvscleansystem_lib in $CLEAN_LIBRARIES; do
		mv "$1/clean-libraries-git/Libraries/$cvscleansystem_lib" "$1/libraries/$cvscleansystem_lib"
	done
	mv "$1/clean-libraries-git/Libraries/ArgEnvWindows" "$1/libraries/ArgEnvWindows"
#	svn_get "$1" clean-code-generator/trunk CodeGenerator
	git_get "$1" clean-compiler-and-rts/code-generator.git CodeGenerator
#	svn_get "$1" clean-run-time-system/trunk RuntimeSystem
	git_get "$1" clean-compiler-and-rts/run-time-system.git RuntimeSystem
#	svn_get "$1" clean-compiler/trunk compiler
	git_get "$1" clean-compiler-and-rts/compiler.git compiler
	svn_get "$1" clean-dynamic-system/trunk dynamic
#	svn_get "$1" clean-ide/trunk tools/CleanIDE
	git_get "$1" clean-and-itasks/clean-ide.git tools/CleanIDE
#	svn_get "$1" clean-language-report/trunk LanguageReport
	git_get "$1" clean-compiler-and-rts/language-report.git LanguageReport
#	cvsget "$1" tools/Sparkle
	svn_get "$1" clean-tools/trunk/htoclean tools/htoclean
#	svn_get "$1" clean-libraries/trunk/Libraries/StdEnv libraries/StdEnv
#	svn_get "$1" clean-libraries/trunk/Libraries/ObjectIO libraries/ObjectIO
	mv "$1/clean-libraries-git/Libraries/ObjectIO" "$1/libraries/ObjectIO"
#	svn_get "$1" clean-libraries/trunk/Examples/ObjectIO CleanExamples/ObjectIO
	mv "$1/clean-libraries-git/Examples/ObjectIO" "$1/CleanExamples/ObjectIO"
#	svn_get "$1" clean-libraries/trunk/Examples/TCPIP CleanExamples/TCPIP
	mv "$1/clean-libraries-git/Examples/TCPIP" "$1/CleanExamples/TCPIP"
#	svn_get "$1" clean-libraries/trunk/Libraries/GenLib libraries/GenLib
	mv "$1/clean-libraries-git/Libraries/GenLib" "$1/libraries/GenLib"
#	svn_get "$1" clean-libraries/trunk/Examples/GenLib CleanExamples/Generics
	mv "$1/clean-libraries-git/Examples/GenLib" "$1/CleanExamples/Generics"
	svn_get "$1" gast libraries/Gast
#	svn_get "$1" clean-examples/trunk CleanExamples/Small\ Examples
#	svn_get "$1" clean-libraries/trunk/Examples/Small\ Examples CleanExamples/Small\ Examples
	mv "$1/clean-libraries-git/Examples/Small Examples" "$1/CleanExamples/Small Examples"
#	for cvscleansystem_lib in $CLEAN_LIBRARIES; do
#		svn_get "$1" "clean-libraries/trunk/Libraries/$cvscleansystem_lib" "libraries/$cvscleansystem_lib"
#	done
#	svn_get "$1" clean-libraries/trunk/Libraries/ArgEnvWindows libraries/ArgEnvWindows
	git_get "$1" clean-and-itasks/clean-platform.git libraries/Platform

	xmakedir "$1/fonts"
	curl "http://git.ghostscript.com/?p=urw-core35-fonts.git;a=blob_plain;f=NimbusMonoPS-Regular.ttf;hb=refs/heads/master" -H "Host: git.ghostscript.com" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" -H "Accept-Language: en-US,en;q=0.5" --compressed -H "DNT: 1" -H "Connection: keep-alive" -H "Upgrade-Insecure-Requests: 1" > "$1/fonts/NimbusMonoPS-Regular.ttf"
	curl "http://git.ghostscript.com/?p=urw-core35-fonts.git;a=blob_plain;f=NimbusMonoPS-Bold.ttf;hb=refs/heads/master"    -H "Host: git.ghostscript.com" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" -H "Accept-Language: en-US,en;q=0.5" --compressed -H "DNT: 1" -H "Connection: keep-alive" -H "Upgrade-Insecure-Requests: 1" > "$1/fonts/NimbusMonoPS-Bold.ttf"
	(cd "$1/fonts"; wget "https://get.fontspace.co/download/family/8j4z/36725246f64c4594a48441c88f803e0b/liberation-sans-font.zip")
	(cd "$1/fonts"; "/c/Program Files/7-Zip/7z.exe" e liberation-sans-font.zip LiberationSansBold-1adM.ttf LiberationSansItalic-RJre.ttf LiberationSans-BaDn.ttf)
	mv "$1/fonts/LiberationSans-BaDn.ttf" "$1/fonts/LiberationSans-Regular.ttf"
	mv "$1/fonts/LiberationSansBold-1adM.ttf" "$1/fonts/LiberationSans-Bold.ttf"
	mv "$1/fonts/LiberationSansItalic-RJre.ttf" "$1/fonts/LiberationSans-Italic.ttf"
}

xduplicate ()
	# $1: source path
	# $2: destination path
{
	log "duplicating '$1' -> '$2'"

	if test ! -e "$1"; then
		fatal "xduplicate: source '$1' does not exist"
	fi
	if test ! -e "$2"; then
		cp -r "$1" "$2"
		checkreturncode "xduplicate" "copying '$1' to '$2'" $?
	fi
}

xduplicate2 ()
	# $1: source path
	# $2: destination dir
{
	log "duplicating '$1' -> '$2'"

	if test ! -e "$1"; then
		fatal "xduplicate2: source '$1' does not exist"
	fi
	if test ! -e "$2"; then
		fatal "xduplicate2: destination '$2' does not exist"
	fi

	xduplicate2_file=`basename "$1"`
	xduplicate "$1" "$2/$xduplicate2_file"
}

xduplicate3 ()
	# $1: source path
	# $2: destination path
{
	log "duplicating '$1' -> '$2'"

	if test ! -e "$1"; then
		fatal "xduplicate3: source '$1' does not exist"
	fi
	if test ! -e "$2"; then
		fatal "xduplicate3: destination '$2' does not exist"
	fi
	cp -r -t "$2" "$1"/*
	checkreturncode "xduplicate3" "copying '$1' to '$2'" $?
}

xduplicatefiles ()
	# $1: source path
	# $2: destination path
{
	log "duplicating files '$1' -> '$2'"

	if test ! -e "$1"; then
		fatal "xduplicate: source '$1' does not exist"
	fi
	if test ! -e "$2"; then
		fatal "xduplicate: destination '$2' does not exist"
	fi

	for xduplicatefiles_source in "$1"/*; do
		xduplicatefiles_file=`basename "$xduplicatefiles_source"`
		xduplicate "$xduplicatefiles_source" "$2/$xduplicatefiles_file"
	done
}

xmove ()
	# $1: source path
	# $2: destination path
{
	if test ! -e "$2"; then
		if test ! -e "$1"; then
			fatal "xmove source '$1' does not exist"
		fi
		mv "$1" "$2"
		checkreturncode "xmove" "moving '$1' to '$2'" $?
	fi
}


xremove ()
	# $1: directory or file path
{
	if test -e "$1"; then
		rm -r "$1"
		checkreturncode "xremove" "removing '$1'" $?
	fi
}


# NONCLEAN_COMPONENTS="runtimesystem backend idersrc ioobjects codegen stdenvobj"
NONCLEAN_COMPONENTS="runtimesystem codegen backend ioobjects tcpip idersrc platform"

runtimesystem_build ()
	# $1: cvs dir
	# $2: build dir
{
	xduplicate "$1/RuntimeSystem" "$2/runtimesystem"

	(cd "$2/runtimesystem"
	 ./build_windows_object_files_ml64.bat
	)
}

runtimesystem_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
	for objectfile in _startup0.o _startup1.o _startup1Profile.o _startup1Trace.o _startup2.o _startup3.o _startup4.o ; do
		xduplicate2 "$1/RuntimeSystem/$objectfile" "$2/Libraries/StdEnv/Clean System Files"
#		xduplicate2 "$1/RuntimeSystem/$objectfile" "$2/Libraries/StdEnv Sparkle/Clean System Files"
	done
}

backend_build ()
	# $1: cvs dir
	# $2: build dir
{
	xduplicate "$1/compiler/backendC" "$2/backend"

	CleanCompilerSources_dir=`dirname "$2/backend/CleanCompilerSources"`
	(cd "$2/backend/CleanCompilerSources"
	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@nmake -f Makefile.windows64
EOBATCH
	checkreturncode "backend_build" "making backend" $?
	)
}

backend_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
	xduplicate "$1/backend/backend.dll" "$2/Tools/Clean System 64/backend.dll"
}

idersrc_build ()
	# $1: cvs dir
	# $2: build dir
{
	xduplicate "$1/tools/CleanIDE/WinSupport" "$2/idersrc"

	xremove "$1/tools/CleanIDE/WinSupport/winIde.rsrc"

	(cd "$2/idersrc"
	cat <<EOBATCH | "$COMMAND"
@		call "$VSSETENV" amd64
@		call makeresource64.bat
EOBATCH
	)

	xduplicate "$2/idersrc/winIde.rsrc"  "$1/tools/CleanIDE/WinSupport/winIde.rsrc"
}

idersrc_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
	echo "NOP" 
}

ioobjects_build ()
	# $1: cvs dir
	# $2: build dir
{
	xduplicate "$1/libraries/ObjectIO/ObjectIO/OS Windows/Windows_C_12" "$2/ioobjects"

	for c in "$2"/ioobjects/*.c; do
		c_file=`basename "$c"`
		if test "$c_file" = "cGameLib_121.c" -o "$c_file" = "cOSGameLib_121.c" -o "$c_file" = "cCrossCallGame_121.c"; then
			vscc_directx "$c"
		else
			vscc "$c"
		fi
    done
}

ioobjects_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
	for ioobjects_dist_ioobj in "$1"/ioobjects/*.obj; do \
		ioobj_file=`basename "$ioobjects_dist_ioobj"`

		if test "$ioobj_file" = "cTCP_121.obj" -o "$ioobj_file" = "cCrossCallTCP_121.obj"; then
			xremove "$2/Libraries/Tcp/Clean System Files/$ioobj_file"
			cp "$ioobjects_dist_ioobj" "$2/Libraries/Tcp/Clean System Files/$ioobj_file"			
		else
			if test "$ioobj_file" = "cGameLib_121.obj" -o "$ioobj_file" = "cOSGameLib_121.obj" -o "$ioobj_file" = "cCrossCallGame_121.obj"; then
				xremove "$2/Libraries/GameLib/Clean System Files/$ioobj_file"
				cp "$ioobjects_dist_ioobj" "$2/Libraries/GameLib/Clean System Files/$ioobj_file"			
			else
				xremove "$2/Libraries/ObjectIO/OS Windows/Clean System Files/$ioobj_file"
				cp "$ioobjects_dist_ioobj" "$2/Libraries/ObjectIO/OS Windows/Clean System Files/$ioobj_file"
			fi
		fi
	done
}

tcpip_build ()
	# $1: cvs dir
	# $2: build dir
{
	mkdir -p "$2/tcpip"
	for h_path in "$1/libraries/ObjectIO/ObjectIO/OS Windows/Windows_C_12"/*.h; do
		h_file=`basename "$h_path"`
		if test "$h_file" != "cTCP_121.h" -a "$h_file" != "cCrossCallTCP_121.h"; then
			xduplicate "$1/libraries/ObjectIO/ObjectIO/OS Windows/Windows_C_12/$h_file" "$2/tcpip/$h_file"
		fi
	done
	for c_or_h_path in "$1/libraries/TCPIP/Windows_C"/*.[ch]; do
		c_or_h_file=`basename "$c_or_h_path"`
		xduplicate "$1/libraries/TCPIP/Windows_C/$c_or_h_file" "$2/tcpip/$c_or_h_file"
	done

	for c in "$2"/tcpip/*.c; do
		c_file=`basename "$c"`
		vscc "$c"
    done
}

tcpip_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
	xremove "$2/Libraries/TCPIP/Clean System Files/cTCP_121.obj"
	cp "$1"/tcpip/cTCP_121.obj "$2/Libraries/TCPIP/Clean System Files/cTCP_121.obj"			
	xremove "$2/Libraries/TCPIP/ObjectIO/Clean System Files/cCrossCallTCP_121.obj"
	cp "$1"/tcpip/cCrossCallTCP_121.obj "$2/Libraries/TCPIP/ObjectIO/Clean System Files/cCrossCallTCP_121.obj"			
}

platform_build ()
	# $1: cvs dir
	# $2: build dir
{
	mkdir -p "$2/platform"

	for c_or_h_path in "$1/libraries/Platform/src/cdeps"/*.[ch]; do
		c_or_h_file=`basename "$c_or_h_path"`
		xduplicate "$1/libraries/Platform/src/cdeps/$c_or_h_file" "$2/platform/$c_or_h_file"
	done

	for c in "$2"/platform/*.c; do
		vscc "$c"
    done
}

platform_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
	for c in "$1"/platform/*.c; do
		base_name=`basename "$c" .c`
		xremove "$2/Libraries/Platform/Clean System Files/$base_name.o"
		cp "$1/platform/$base_name.obj" "$2/Libraries/Platform/Clean System Files/$base_name.o"
    done
}

codegen_build ()
	# $1: cvs dir
	# $2: build dir
{
	xduplicate "$1/CodeGenerator" "$2/codegen"

	(cd "$2/codegen"
	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@nmake -f Makefile.windows64
EOBATCH
	checkreturncode "codegen_build" "making cg.exe" $?
	)
}

codegen_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
	xduplicate "$1/codegen/cg.exe" "$2/Tools/Clean System 64/CodeGenerator.exe"
}

stdenvobj_build ()
	# $1: cvs dir
	# $2: build dir
{
	xduplicate "$1/libraries/StdEnv/Object Files Windows 32" "$2/stdenvobj"
}

stdenvobj_dist ()
	# $1: non-Clean build dir
	# $2: dist dir
{
#	xduplicate2 "$1"/stdenvobj/_startup0.o "$2/Libraries/StdEnv/Clean System Files"
	for stdenvobj_dist_object in "$1"/stdenvobj/*_library; do
		xduplicate2 "$stdenvobj_dist_object" "$2/Libraries/StdEnv/Clean System Files"
	done
}

# CLEAN_COMPONENTS="compiler stdenv ide dynamic sparkle htoclean"
CLEAN_COMPONENTS="compiler stdenv ide dynamic htoclean language_report"

cleanduplicate ()
	# $1: source path
	# $2: alternative source path
	# $3: destination path
{
	if test -e "$1"; then
		xduplicate "$1" "$3"
	else
		xduplicate "$2" "$3"
	fi
}

compiler_build ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
	# $4: nonclean dir
{
	xduplicate "$2/compiler" "$3/compiler"

	xduplicate "$3/compiler/backendC/CleanCompilerSources/backend.h" \
				 "$3/compiler/backend/backend.h"

	compiler_build_htoclean="$1/Tools/htoclean/htoclean.exe"

	# allow initial build with 2.1.0 release that has version numbers
	"$1/Tools/htoclean"*/htoclean.exe `cygpath --windows --absolute "$3/compiler/backend/backend.h"`

	xmakedir "$3/compiler/Clean System Files"
	cleanbuild "$1" "$3/compiler/CleanCompiler64.prj"

	xduplicate "$4/backend/backend.dll" "$3/compiler/backend.dll"
}

compiler_dist ()
	# $1: Clean build dir
	# $2: non-Clean build dir
	# $3: dist dir
{
	cleanduplicate "$1/compiler/CleanCompiler64.exe" "$CLEANROOT/Tools/Clean System 64"/CleanCompiler64.exe \
					"$3/Tools/Clean System 64/CleanCompiler64.exe"

}

stdenv_build ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
	xduplicate "$2/libraries/StdEnv" "$3/stdenv"

	for stdenv_64_file_path in "$3/stdenv/StdEnv 64 Changed Files"/*.[id]cl; do
		stdenv_64_file=`basename "$stdenv_64_file_path"`
		xremove "$3/stdenv/$stdenv_64_file"
		cp "$stdenv_64_file_path" "$3/stdenv/$stdenv_64_file"
	done

	for stdenv_64_file_path in "$3/stdenv/StdEnv 64 Changed Files"/*.abc; do
		stdenv_64_file=`basename "$stdenv_64_file_path"`
		xremove "$3/stdenv/Clean System Files/$stdenv_64_file"
		cp "$stdenv_64_file_path" "$3/stdenv/Clean System Files/$stdenv_64_file"
	done

	for stdenvobj_dist_object in "$3/stdenv/Library Files Windows 64"/*_library; do
		xduplicate2 "$stdenvobj_dist_object" "$3/stdenv/Clean System Files"
	done

	# compile twice to inline abc code between system modules
	for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
#		cleancocl "$1" "$3/stdenv/$stdenv_build_m" -dynamic;
#		cleancocl "$1" "$3/stdenv/$stdenv_build_m";
		newcleancocl "$1" "$3/stdenv/$stdenv_build_m" "$3/compiler";
	done
	for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
#		cleancocl "$1" "$3/stdenv/$stdenv_build_m";
		newcleancocl "$1" "$3/stdenv/$stdenv_build_m" "$3/compiler";
	done
	
	xremove "$3/stdenv/Makefile.linux"
	xremove "$3/stdenv/Makefile.solaris"
	xremove "$3/stdenv/StdFile_solaris.icl"
	xremove "$3/stdenv/StdFile_solaris.dcl"
	xremove "$3/stdenv/StdEnv 64 Changed Files"
	xremove "$3/stdenv/Object Files Windows 32"
	xremove "$3/stdenv/Library Files Windows 64"
	xremove "$3/stdenv/Library Files Macintosh"
}

stdenv_dist ()
	# $1: Clean build dir
	# $2: non-Clean build dir
	# $3: dist dir
{
	xduplicate "$1/stdenv" "$3/Libraries/StdEnv"
	
#	for stdenv_dist_m in $STDENV_SYSTEM_MODULES; do
#		xduplicate "$1/stdenv/Clean System Files/$stdenv_dist_m.abc" \
#			"$3/Libraries/StdEnv/Clean System Files/$stdenv_dist_m.abc"
#		xduplicate "$1/stdenv/Clean System Files/$stdenv_dist_m.tcl" \
#			"$3/Libraries/StdEnv/Clean System Files/$stdenv_dist_m.tcl"
#	done	
}

ide_build ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
	# $4: nonclean dir
{
	xduplicate "$2/tools/CleanIDE" "$3/ide"

	for rsrc_file in winIde.rsrc; do
		cp "$4/idersrc/$rsrc_file" "$3/ide/WinSupport/$rsrc_file"
	done

	for ide_build_prj in WinIde64 WinHeapProfile64 WinTimeProfile64 BatchBuild CpmWin; do
		cleanbuild "$1" "$3/ide/$ide_build_prj.prj"
	done
}

ide_dist ()
	# $1: Clean build dir
	# $2: non-Clean build dir
	# $3: dist dir
{
	cleanduplicate "$1/ide/CleanIDE.exe" "$CLEANROOT/CleanIDE.exe" \
						"$3/CleanIDE.exe"
	cleanduplicate "$1/ide/ShowHeapProfile.exe" "$CLEANROOT"/Tools/Heap\ Profiler/ShowHeapProfile.exe \
						"$3/Tools/Heap Profiler/ShowHeapProfile.exe"
	cleanduplicate "$1/ide/ShowTimeProfile.exe" "$CLEANROOT"/Tools/Time\ Profiler/ShowTimeProfile.exe \
						"$3/Tools/Time Profiler/ShowTimeProfile.exe"
	cleanduplicate "$1/ide/BatchBuild.exe" "$CLEANROOT/BatchBuild.exe" \
						"$3/BatchBuild.exe"
	cleanduplicate "$1/ide/cpm/cpm.exe" "$CLEANROOT/cpm/cpm.exe" \
						"$3/cpm.exe"
}

sparkle_build ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
	xduplicate "$2/tools/Sparkle" "$3/sparkle"
	xduplicate "$2/compiler/frontend" "$3/sparkle/Libraries/frontend"

	cleanbuild "$1" "$3/sparkle/Sparkle.prj"

	# compile twice to inline abc code between system modules
	for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
		cleancocl "$1" "$3/sparkle/libraries/StdEnv Sparkle 0.0.4a/$stdenv_build_m" -dynamic;
	done
	for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
		cleancocl "$1" "$3/sparkle/libraries/StdEnv Sparkle 0.0.4a/$stdenv_build_m";
	done
}

sparkle_dist ()
	# $1: Clean build dir
	# $2: non-Clean build dir
	# $3: dist dir
{
	cleanduplicate "$1/sparkle/Tools/Sparkle.exe" "$CLEANROOT"/Tools/Sparkle*/Sparkle.exe \
						"$3/Tools/Sparkle/Sparkle.exe"

	mkdir -p "$3/Libraries/StdEnv Sparkle/Clean System Files"

	for stdenv_dist_m in $STDENV_SYSTEM_MODULES; do
		xduplicate "$1/sparkle/libraries/StdEnv Sparkle 0.0.4a/Clean System Files/$stdenv_dist_m.abc" \
			"$3/Libraries/StdEnv Sparkle/Clean System Files/$stdenv_dist_m.abc"
		xduplicate "$1/sparkle/libraries/StdEnv Sparkle 0.0.4a/Clean System Files/$stdenv_dist_m.tcl" \
			"$3/Libraries/StdEnv Sparkle/Clean System Files/$stdenv_dist_m.tcl"
	done	

#	xduplicate2 "$3/Libraries/StdEnv/Clean System Files/_system.abc" "$3/Libraries/StdEnv Sparkle/Clean System Files"
#	xduplicate2 "$2"/stdenvobj/_startup0.o "$3/Libraries/StdEnv Sparkle/Clean System Files"
#	for stdenvobj_dist_object in "$2"/stdenvobj/*_library; do
#		xduplicate2 "$stdenvobj_dist_object" "$3/Libraries/StdEnv Sparkle/Clean System Files"
#	done
}

dynamic_build ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
	xduplicate "$2/dynamic" "$3/dynamic"

#	for dynamic_build_prj in  checkdynamics dumpDynamic dynGarbCollector \
#									StaticLinker DynamicLinker; do
	for dynamic_build_prj in "StaticLinker 64"; do
		cleanbuild "$1" "$3/dynamic/$dynamic_build_prj.prj"
	done
}

dynamic_dist ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
#	cleanduplicate "$1/dynamic/checkdynamics.exe" "$CLEANROOT"/Tools/Dynamics*/utilities/checkdynamics*.exe \
#						"$3"/Tools/Dynamics/utilities/checkdynamics.exe
#	cleanduplicate "$1/dynamic/dumpDynamic.exe" "$CLEANROOT"/Tools/Dynamics*/utilities/dumpDynamic*.exe \
#						"$3"/Tools/Dynamics/utilities/dumpDynamic.exe
#	cleanduplicate "$1/dynamic/dynGarbCollector.exe" "$CLEANROOT"/Tools/Dynamics*/utilities/dynGarbCollector*.exe \
#						"$3"/Tools/Dynamics/utilities/dynGarbCollector.exe

	cleanduplicate "$1/dynamic/StaticLinker.exe" "$CLEANROOT"/Tools/Clean\ System\ 64/StaticLinker.exe \
						"$3"/Tools/Clean\ System\ 64/StaticLinker.exe

#	cleanduplicate "$1/dynamic/DynamicLinker.exe" "$CLEANROOT"/Tools/Dynamics*/DynamicLinker*.exe \
#						"$3"/Tools/Dynamics/DynamicLinker.exe
}

htoclean_build ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
	xduplicate "$2/tools/htoclean/htoclean source code" "$3/htoclean"

	cleanbuild "$1" "$3/htoclean/htoclean.prj"
}

htoclean_dist ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
	cleanduplicate "$1/htoclean/htoclean.exe" "$CLEANROOT"/Tools/htoclean*/htoclean*.exe \
						"$3/Tools/htoclean/htoclean.exe"	
}

language_report_build ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
	xduplicate "$2/LanguageReport/write_clean_manual" "$3/write_clean_manual"

	xduplicate2 "$2/fonts/NimbusMonoPS-Regular.ttf" "$3/write_clean_manual"
	xduplicate2 "$2/fonts/NimbusMonoPS-Bold.ttf" "$3/write_clean_manual"
	xduplicate2 "$2/fonts/LiberationSans-Regular.ttf" "$3/write_clean_manual"
	xduplicate2 "$2/fonts/LiberationSans-Bold.ttf" "$3/write_clean_manual"
	xduplicate2 "$2/fonts/LiberationSans-Italic.ttf" "$3/write_clean_manual"

	cleanbuild "$1" "$3/write_clean_manual/write_clean_manual.prj"

	(cd "$3/write_clean_manual"; "./write_clean_manual.exe")
}

language_report_dist ()
	# $1: clean system dir
	# $2: cvs dir
	# $3: build dir
{
	xduplicate "$1"/write_clean_manual/CleanLanguageReport.pdf "$3/Help/CleanLanguageReport.pdf"
	xmakedir "$3/Help/CleanLanguageReportHtml"
	for html_path in "$1"/write_clean_manual/*.html ; do
		html_file=`basename "$html_path"`
		if test "$html_file" != "CleanLanguageReport.html"; then
			if test "$html_file" != "CleanLanguageReportT.html"; then
				xduplicate "$1/write_clean_manual/$html_file" "$3/Help/CleanLanguageReportHtml/$html_file"
			else
				xduplicate "$1/write_clean_manual/$html_file" "$3/Help/CleanLanguageReportHtml/CleanLanguageReport.html"
			fi
		fi
	done
}

buildnonclean ()
	# $1: directory with sources from cvs
	# $2: directory to build components that are not written in Clean
{
	xmakedir "$2"

	for buildnonclean_component in $NONCLEAN_COMPONENTS; do
		${buildnonclean_component}_build "$1" "$2" $buildnonclean_component
	done
}

buildclean ()
	# $1: clean system dir
	# $2: directory with sources from cvs
	# $3: nonclean dir
	# $4: directory to build components that are written in Clean
{
	xmakedir "$4"

	for buildclean_component in $CLEAN_COMPONENTS; do
		${buildclean_component}_build "$1" "$2" "$4" "$3" $buildclean_component
	done
}

distcreatedirs ()
	# $1: directory to put distribution in
{
	xmakedir "$1"
	xmakedir "$1/Libraries"
	xmakedir "$1/Examples"
	xmakedir "$1/Temp"
	xmakedir "$1/Tools"
	xmakedir "$1/Tools/Clean System 64"
	xmakedir "$1/Tools/Heap Profiler"
	xmakedir "$1/Tools/Time Profiler"
#	xmakedir "$1/Tools/Dynamics"
#	xmakedir "$1/Tools/Dynamics/conversion"
#	xmakedir "$1/Tools/Dynamics/utilities"
#	xmakedir "$1/Tools/Sparkle"
}

distduplicate ()
	# $1: directory with sources from cvs
	# $2: directory to put distribution in
{
	# Root
	xduplicate "$1/tools/CleanIDE/CleanLicenseConditions.txt" "$2/CleanLicenseConditions.txt"

	# Config
	xduplicate "$1"/tools/CleanIDE/Config "$2"/Config
	xremove "$2"/Config/IDEEnvs
	xduplicate "$1"/tools/CleanIDE/Config/"Windows 64"/IDEEnvs "$2"/Config/IDEEnvs
	xremove "$2"/Config/Macintosh
	xremove "$2"/Config/"Windows 64"
	xduplicate "$2"/Config/windows.km "$2"/Config/default.km

	# Tools
	xduplicate "$1/tools/htoclean" "$2/Tools/htoclean"
	xduplicate "$1/tools/CleanIDE/HeapProfile/ShowHeapProfileHelp" "$2/Tools/Heap Profiler/ShowHeapProfileHelp"
	xduplicate "$1/tools/CleanIDE/TimeProfile/ShowTimeProfileHelp" "$2/Tools/Time Profiler/ShowTimeProfileHelp"
#	xduplicate "$1/tools/Sparkle/Tools/Changes.txt" "$2/Tools/Sparkle/Changes.txt"
#	xduplicate "$1/tools/Sparkle/Tools/options" "$2/Tools/Sparkle/options"
#	xduplicate "$1/tools/Sparkle/Tools/Images" "$2/Tools/Sparkle/Images"
#	xduplicate "$1/tools/Sparkle/Tools/Sections" "$2/Tools/Sparkle/Sections"

	# Libraries
	for distduplicate_library in $CLEAN_LIBRARIES; do
		xduplicate "$1/libraries/$distduplicate_library" "$2/Libraries/$distduplicate_library"
	done
	xduplicate "$1/libraries/ArgEnvWindows" "$2/Libraries/ArgEnv"
	xduplicate "$1/libraries/Gast" "$2/Libraries/Gast"
	xmove "$2/Libraries/Directory/Clean System Files Windows" \
			"$2/Libraries/Directory/Clean System Files"
#	xmove "$2/Libraries/ExtendedArith/ExtendedArith/Clean System Files Windows" \
#			"$2/Libraries/ExtendedArith/ExtendedArith/Clean System Files"
	xduplicate "$1/libraries/ObjectIO/ObjectIO" "$2/Libraries/ObjectIO"
	xduplicate "$1/libraries/ObjectIO/GameLib" "$2/Libraries/GameLib"
	xduplicate "$1/libraries/ObjectIO/Tcp" "$2/Libraries/Tcp"
	xremove "$2/Libraries/Tcp/Macintosh"
	xduplicate "$1"/libraries/GenLib "$2/Libraries/Generics"
#	xduplicate "$1"/dynamic/dynamics/StdDynamicEnv "$2/Libraries/Dynamics"
	xmakedir "$2/Libraries/Dynamics"
	xduplicate2 "$1"/dynamic/dynamics/StdDynamicEnv/implementation/_SystemDynamic.dcl "$2/Libraries/Dynamics"
	xduplicate2 "$1"/dynamic/dynamics/StdDynamicEnv/implementation/_SystemDynamic.icl "$2/Libraries/Dynamics"
	xduplicate2 "$1"/dynamic/dynamics/StdDynamicEnv/extension/StdCleanTypes.dcl "$2/Libraries/Dynamics"
	xduplicate2 "$1"/dynamic/dynamics/StdDynamicEnv/extension/StdCleanTypes.icl "$2/Libraries/Dynamics"
	xduplicate2 "$1"/dynamic/dynamics/StdDynamicEnv/extension/StdDynamic.dcl "$2/Libraries/Dynamics"
	xduplicate "$1"/dynamic/dynamics/StdDynamicEnv/extension/StdDynamicNoLinker.icl "$2/Libraries/Dynamics/StdDynamic.icl"

	xduplicate "$1/libraries/Platform/src/libraries/OS-Independent" "$2/Libraries/Platform"
	xduplicate3 "$1/libraries/Platform/src/libraries/OS-Windows" "$2/Libraries/Platform"
	xduplicate3 "$1/libraries/Platform/src/libraries/OS-Windows-64" "$2/Libraries/Platform"
	xduplicate3 "$1/libraries/Platform/src/libraries/Platform-x86" "$2/Libraries/Platform"

#	xduplicate "$1/tools/Sparkle/Libraries/StdEnv Sparkle 0.0.4a" "$2/Libraries/StdEnv Sparkle"

	xremove "$2/Libraries/Directory/Clean System Files Macintosh"
#	xremove "$2/Libraries/ExtendedArith/ExtendedArith/Clean System Files Macintosh"

#	xremove "$2/Libraries/StdEnv/Makefile.linux"
#	xremove "$2/Libraries/StdEnv/Makefile.solaris"
#	xremove "$2/Libraries/StdEnv/StdFile_solaris.icl"
#	xremove "$2/Libraries/StdEnv/StdFile_solaris.dcl"
#	xremove "$2/Libraries/StdEnv/StdEnv 64 Changed Files"
#	xremove "$2/Libraries/StdEnv/Object Files Windows 32"
#	xremove "$2/Libraries/StdEnv/Library Files Windows 64"
#	xremove "$2/Libraries/StdEnv/Library Files Macintosh"

	xremove "$2/Libraries/ObjectIO/OS Linux"
	xremove "$2/Libraries/ObjectIO/OS Mac Carbon"

#	xmakedir "$2/Libraries/Hilde/OS Windows/Clean System Files"
#	xduplicate "$1/Libraries/Hilde/OS Windows/Foreign/windowsKERNEL32_library" "$2/Libraries/Hilde/OS Windows/Clean System Files/windowsKERNEL32_library"
#	xduplicate "$1/Libraries/Hilde/OS Windows/Foreign/windowsWS2_32_library" "$2/Libraries/Hilde/OS Windows/Clean System Files/windowsWS2_32_library"
#	xremove "$2/Libraries/Hilde/WrapDebugClosures"

	for distduplicate_ioobj in \
		"$2/LibrariesObjectIO/OS Windows/Clean System Files"/*.o \
		"$2/LibrariesObjectIO/OS Windows/Clean System Files"/*.obj; do
		xremove "$distduplicate_ioobj"
	done
	xremove "$2/Libraries/Gast/trunk"
	xremove "$2/Libraries/Gast/branches"
	xremove "$2/Libraries/Gast/tags"

	# Examples
	xduplicate "$1"/CleanExamples/Generics "$2"/Examples/Generics
	xduplicate "$1"/CleanExamples/Small\ Examples "$2"/Examples/Small\ Examples
	xremove "$2"/Examples/Small\ Examples/Macintosh
#	xduplicate "$1/libraries/ObjectIO/ObjectIO Examples" "$2/Examples/ObjectIO Examples"
	xduplicate "$1/CleanExamples/ObjectIO/ObjectIO Examples" "$2/Examples/ObjectIO Examples"
	for a in "$2/Examples/ObjectIO Examples/"*; do
		xremove "$a/Macintosh"
	done
#	xduplicate "$1/libraries/ObjectIO/Game Examples" "$2/Examples/Game Examples"
	xduplicate "$1/CleanExamples/ObjectIO/Game Examples" "$2/Examples/Game Examples"
#	xduplicate "$1/libraries/ObjectIO/Tcp Examples" "$2/Examples/Tcp Examples"
	xduplicate "$1/CleanExamples/TCPIP" "$2/Examples/Tcp Examples"
	for a in "$2/Examples/Tcp Examples/"*; do
		xremove "$a/Macintosh"
	done
#	xmove "$2/Libraries/GEC/examples" "$2/Examples/GEC"
#	xduplicate "$1/dynamic/dynamics/Examples/User examples" "$2/Examples/Dynamics"
	xduplicate "$1/dynamic/dynamics/Examples/Examples No Linker" "$2/Examples/Dynamics"

	xmakedir "$2/Examples/Parsers"
	xmove "$2/Libraries/Parsers/ParsersTestware.icl" "$2/Examples/Parsers/ParsersTestware.icl"
	xmove "$2/Libraries/Parsers/ParsersTestware.prj" "$2/Examples/Parsers/ParsersTestware.prj"
	xmove "$2/Libraries/Parsers/MetarDemo" "$2/Examples/Parsers/MetarDemo"

	xmove "$2/Libraries/Gast/examples" "$2/Examples/Gast"
	xremove "$2/Examples/Gast/testFixed.icl"
	xremove "$2/Examples/Gast/testFixed.prj"
	xremove "$2/Examples/Gast/fixed.icl"
	xremove "$2/Examples/Gast/fixed.dcl"

	xduplicate "$1/libraries/Platform/src/examples" "$2/Examples/Platform Examples"
	find "$2/Examples/Platform Examples" -name .gitignore -delete

	# Help
	xduplicate "$1"/tools/CleanIDE/Help "$2"/Help

	xmove "$2/Tools/htoclean/CallingCFromClean.html" "$2/Help/CallingCFromClean.html"
	xmove "$2/Libraries/Parsers/Parsers Manual.doc" "$2/Help/Library Documents/Parsers Manual.doc"
}

dist ()
	# $1: directory with sources from cvs
	# $2: directory with built nonclean components
	# $3: directory with built clean components
	# $4: directory to put distribution in
{
	distcreatedirs "$4"
	distduplicate "$1" "$4"

	for dist_component in $CLEAN_COMPONENTS; do
		${dist_component}_dist  "$3" "$2" "$4"
	done

	for dist_component in $NONCLEAN_COMPONENTS; do
		${dist_component}_dist "$2" "$4"
	done
}

# main

CVS_SOURCE="$ROOT/cvssource"

cvscleansystem "$CVS_SOURCE"

if grep -q SystemTimeToTzSpecificLocalTime "./Clean_3.0_64/Libraries/StdEnv/Clean System Files/kernel_library"; then
	:
else
	if [[ $(tail -c 1 "./Clean_3.0_64/Libraries/StdEnv/Clean System Files/kernel_library" | wc -l) -eq 0 ]]; then
		echo "" >> "./Clean_3.0_64/Libraries/StdEnv/Clean System Files/kernel_library"
	fi
	echo "SystemTimeToTzSpecificLocalTime" >> "./Clean_3.0_64/Libraries/StdEnv/Clean System Files/kernel_library"
fi

if test 1 -ne 0; then

buildnonclean "$CVS_SOURCE" "$ROOT/nonclean64"

buildclean "$CLEANROOT" "$CVS_SOURCE" "$ROOT/nonclean64" "$ROOT/step64_0/clean"
dist "$CVS_SOURCE" "$ROOT/nonclean64" "$ROOT/step64_0/clean" "$ROOT/step64_0/dist"

buildclean "$ROOT/step64_0/dist" "$CVS_SOURCE" "$ROOT/nonclean64" "$ROOT/step64_1/clean"
dist "$CVS_SOURCE" "$ROOT/nonclean64" "$ROOT/step64_1/clean" "$ROOT/step64_1/dist"

buildclean "$ROOT/step64_1/dist" "$CVS_SOURCE" "$ROOT/nonclean64" "$ROOT/step64_2/clean"
dist "$CVS_SOURCE" "$ROOT/nonclean64" "$ROOT/step64_2/clean" "$ROOT/step64_2/dist"

buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples"
#buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples/Small Examples"
#buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples/ObjectIO Examples"
#buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples/Generics"
#buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples/Dynamics"
#buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples/Parsers"
#buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples/Tcp Examples"
#buildallcleanprojectfiles "$ROOT/step64_1/dist"  "$ROOT/step64_1/dist/Examples/Game Examples"

fi

log "Build successful!!!"

(cd "$ROOT"; "$BUILDDIR"/adjust_projects_for_64bit.sh)

mkdir -p target/clean-classic
mv "$ROOT/step64_2/dist"/* target/clean-classic/
