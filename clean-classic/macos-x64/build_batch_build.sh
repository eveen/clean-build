set -e
./git_clone.sh clean-and-itasks/clean-ide.git clean-ide

cd clean-ide/BatchBuild
sh make.macosx.sh
#clm -IL ArgEnv -IL StdLib -IL Directory -I BatchBuild -I Pm -I MacOSX -I Util -s 2m -h 20m BatchBuild -o batch_build
cd ..
clm -nt -nr -I cpm -I cpm/Posix -I BatchBuild -I Pm -I Util -I MacOSX -I Interfaces/LinkerInterface -IL ArgEnv -IL StdLib -IL Directory Cpm -o cpm/cpm
cd ..
