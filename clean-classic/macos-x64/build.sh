set -e
INITPWD=`pwd`
export BOOTCOMPILERPATH=$INITPWD/boot_compiler/bin:$INITPWD/boot_compiler/lib/exe:$PATH
cd ./clean-classic/macos-x64
PWD=`pwd`
export OLDPATH=$PATH
./build_cg.sh
./build_clm.sh
./build_rts.sh
export PATH=$BOOTCOMPILERPATH
./build_htoclean.sh
./build_clc.sh
./build_batch_build.sh
./build_libraries.sh
./build_clean.sh
./build_stdenv.sh
./build_examples.sh
export PATH=$OLDPATH
mv clean clean0
cd clean0
make
cd ..
export PATH=$PWD/clean0:$PWD/clean0/bin:$PWD/clean0/lib/exe:$OLDPATH
mkdir step0
mkdir step0/clean-libraries
mkdir step0/clean-libraries/Libraries
mv stdenv step0/stdenv
mv clean-libraries/Libraries/ArgEnvUnix step0/clean-libraries/Libraries/ArgEnvUnix
mv clean-libraries/Libraries/GenLib step0/clean-libraries/Libraries/GenLib
mv clean-libraries/Libraries/StdLib step0/clean-libraries/Libraries/StdLib
mv compiler step0/compiler
mv language-report step0/language-report
mkdir step0/tools
mv tools/htoclean step0/tools/htoclean
mkdir step0/clean-ide
# mv clean-ide/CleanLicenseConditions.txt step0/clean-ide/CleanLicenseConditions.txt
mv clean-ide step0/clean-ide
# mkdir step0/clean-ide/Help
# mv tools/CleanIDE/Help/CleanLangRep.2.1.pdf step0/tools/CleanIDE/Help/CleanLangRep.2.1.pdf
mkdir step0/clean-libraries/Examples
mv "clean-libraries/Examples/Small Examples" "step0/clean-libraries/Examples/Small Examples"
./build_htoclean.sh
./build_clc.sh
./build_batch_build.sh
./build_libraries.sh
./build_clean.sh
./build_stdenv.sh
./build_examples.sh

cd ../..
mkdir -p target/clean-classic
mv clean-classic/macos-x64/clean/* target/clean-classic/
