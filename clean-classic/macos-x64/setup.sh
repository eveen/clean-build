#!/bin/sh
#curl -L -o clean.zip https://ftp.cs.ru.nl/Clean/nightly/clean-itasks-osx-20160630.zip
curl -L -o clean.zip https://ftp.cs.ru.nl/Clean/Clean30/macosx/clean3.0.zip
unzip clean.zip
mv clean boot_compiler
rm clean.zip
rmdir clean

cd boot_compiler
make
cd ..
