
if test ! -d clean-libraries ; then
  # ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
  ./git_clone.sh clean-and-itasks/clean-libraries.git clean-libraries
else
  (cd clean-libraries; git checkout Libraries/ArgEnvUnix Libraries/GenLib Libraries/StdLib)
fi
# ./svn_checkout.sh clean-libraries/trunk/Libraries/GenLib libraries/GenLib
# ./svn_checkout.sh clean-libraries/trunk/Libraries/StdLib libraries/StdLib
# ./svn_checkout.sh clean-libraries/trunk/Libraries/Directory libraries/Directory

cp tools/htoclean/Clean.h "clean-libraries/Libraries/Directory/Clean System Files Unix/Clean.h"
(cd "clean-libraries/Libraries/Directory/Clean System Files Unix"; gcc -c -O cDirectory.c)

# ./svn_checkout.sh clean-libraries/trunk/Libraries/MersenneTwister libraries/MersenneTwister
# ./svn_checkout.sh clean-libraries/trunk/Libraries/StdLib libraries/StdLib
# ./svn_checkout.sh clean-libraries/trunk/Libraries/TCPIP libraries/TCPIP

cp tools/htoclean/Clean.h "clean-libraries/Libraries/TCPIP/Linux_C/Clean.h"
(cd "clean-libraries/Libraries/TCPIP/Linux_C"; gcc -c -O cTCP_121.c)
(cd "clean-libraries/Libraries/TCPIP"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)
(cd "clean-libraries/Libraries/TCPIP/Linux_C"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)

./svn_checkout.sh clean-dynamic-system/trunk/dynamics/StdDynamicEnv StdDynamicEnv

cp -R Gast clean-libraries/Libraries
