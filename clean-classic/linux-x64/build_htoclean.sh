if test ! -d clean-libraries ; then
  # ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
  ./git_clone.sh clean-and-itasks/clean-libraries.git clean-libraries
else
  (cd clean-libraries; git checkout Libraries/ArgEnvUnix)
fi
./svn_checkout.sh clean-tools/trunk/htoclean tools/htoclean

( cd clean-libraries/Libraries/ArgEnvUnix ; make -f Makefile ArgEnvC.o )

cd tools/htoclean/htoclean\ source\ code
clm -I unix -I ../../../clean-libraries/Libraries/ArgEnvUnix -h 4m -nt -nr htoclean -o htoclean
cd ../../..

