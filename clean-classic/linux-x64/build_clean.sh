
#svn checkout https://svn.cs.ru.nl/repos/clean-ide/trunk tools/CleanIDE
#svn checkout https://svn.cs.ru.nl/repos/clean-language-report/trunk language_report
./git_clone.sh clean-compiler-and-rts/language-report.git language-report

mkdir -p clean
cp clean-ide/CleanLicenseConditions.txt clean
cp txt/Makefile clean
cp txt/README clean

mkdir -p clean/bin
cp tools/clm/clm clean/bin
cp tools/clm/patch_bin clean/bin
cp tools/htoclean/htoclean\ source\ code/htoclean clean/bin
cp clean-ide/BatchBuild/BatchBuild clean/bin
cp clean-ide/cpm/cpm clean/bin

mkdir -p clean/exe
cp compiler/cocl clean/exe
cp CodeGenerator/cg clean/exe
cp tools/elf_linker/linker clean/exe

mkdir -p clean/doc
cp language-report/CleanLangRep.2.2.pdf clean/doc
mkdir -p clean/doc/CleanLangRep
cp language-report/*.htm clean/doc/CleanLangRep
cp language-report/*.css clean/doc/CleanLangRep
cp language-report/*.png clean/doc/CleanLangRep
mkdir -p clean/doc/CleanLangRep/CleanRep.2.2_files
cp language-report/CleanRep.2.2_files/* clean/doc/CleanLangRep/CleanRep.2.2_files
cp tools/htoclean/Clean.h clean/doc
cp tools/htoclean/CallingCFromClean.html clean/doc
mkdir -p clean/doc/Examples
cp -r tools/htoclean/Examples/*.[ch] clean/doc/Examples
cp -r tools/htoclean/Examples/*.[id]cl clean/doc/Examples
cp -r tools/htoclean/Examples/*.prj clean/doc/Examples
cp -r tools/htoclean/Examples/*.bat clean/doc/Examples

mkdir -p clean/man/man1
cp txt/clm.1 clean/man/man1

mkdir -p clean/data/ArgEnv
for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README ; do
	cp "clean-libraries/Libraries/ArgEnvUnix/$f" "clean/data/ArgEnv/$f"
done

mkdir -p clean/data/Generics
cp clean-libraries/Libraries/GenLib/*.[id]cl clean/data/Generics/

mkdir -p clean/data/Gast
cp clean-libraries/Libraries/Gast/*.[id]cl clean/data/Gast/

mkdir -p clean/data/StdLib
cp clean-libraries/Libraries/StdLib/*.[id]cl clean/data/StdLib/

mkdir -p "clean/data/Directory/Clean System Files"
cp clean-libraries/Libraries/Directory/* clean/data/Directory
cp "clean-libraries/Libraries/Directory/Clean System Files Unix"/* "clean/data/Directory/Clean System Files"

mkdir -p "clean/data/MersenneTwister/Clean System Files"
cp clean-libraries/Libraries/MersenneTwister/*.* clean/data/MersenneTwister

mkdir -p "clean/data/TCPIP/Clean System Files"
cp clean-libraries/Libraries/TCPIP/TCPIP.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPIP.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPDef.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPDef.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPEvent.dcl	clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPEvent.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannelClass.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannelClass.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannels.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannels.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannels.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannels.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannelsInternal.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannelsInternal.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/tcp_bytestreams.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/tcp_bytestreams.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/tcp.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/tcp.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/ostcp.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/ostcp.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/cTCP_121.o "clean/data/TCPIP/Clean System Files"
cp clean-libraries/Libraries/TCPIP/Linux_C/cTCP_121.c "clean/data/TCPIP/Clean System Files"
cp clean-libraries/Libraries/TCPIP/Linux_C/cTCP_121.h "clean/data/TCPIP/Clean System Files"

mkdir -p "clean/data/Dynamics/Clean System Files"
cp StdDynamicEnv/extension/StdCleanTypes.dcl clean/data/Dynamics
cp StdDynamicEnv/extension/StdCleanTypes.icl clean/data/Dynamics
cp StdDynamicEnv/extension/StdDynamic.dcl clean/data/Dynamics
cp StdDynamicEnv/extension/StdDynamicNoLinker.icl clean/data/Dynamics/StdDynamic.icl
cp StdDynamicEnv/implementation/_SystemDynamic.dcl clean/data/Dynamics
cp StdDynamicEnv/implementation/_SystemDynamic.icl clean/data/Dynamics
