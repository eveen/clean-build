# ./svn_checkout.sh clean-libraries/trunk/Libraries/StdEnv libraries/StdEnv
./git_clone.sh clean-compiler-and-rts/stdenv.git stdenv

mkdir -p clean/StdEnv
cp stdenv/_library.dcl clean/StdEnv
cp stdenv/_startup.dcl clean/StdEnv
cp stdenv/_system.dcl clean/StdEnv
for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	 StdFunc StdFunctions StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	 StdStrictLists StdTuple _SystemArray _SystemEnum _SystemEnumStrict \
	 _SystemStrictLists StdGeneric StdMaybe _SystemStrictMaybes;
do cp stdenv/$a.[di]cl clean/StdEnv ;
done

cp stdenv/StdInt.icl clean/StdEnv
cp stdenv/StdEnv\ 64\ Changed\ Files/StdInt.dcl clean/StdEnv
cp stdenv/StdFile.dcl clean/StdEnv
cp stdenv/StdEnv\ 64\ Changed\ Files/StdFile.icl clean/StdEnv
cp stdenv/StdReal.dcl clean/StdEnv
cp stdenv/StdEnv\ 64\ Changed\ Files/StdReal.icl clean/StdEnv
cp stdenv/StdString.dcl clean/StdEnv
cp stdenv/StdEnv\ 64\ Changed\ Files/StdString.icl clean/StdEnv

cp txt/_startupProfile.dcl clean/StdEnv
cp txt/_startupTrace.dcl clean/StdEnv
cp txt/Makefile_stdenv clean/StdEnv/Makefile
cp txt/make.sh clean/StdEnv/make.sh
cp txt/install.sh clean/StdEnv/install.sh

mkdir -p clean/StdEnv/Clean\ System\ Files
cp stdenv/StdEnv\ 64\ Changed\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
cp RuntimeSystem/linux64/_startup.o clean/StdEnv/Clean\ System\ Files
cp RuntimeSystem/linux64Profile/_startupProfile.o clean/StdEnv/Clean\ System\ Files
cp RuntimeSystem/linux64Trace/_startupTrace.o clean/StdEnv/Clean\ System\ Files

clean/exe/cg clean/StdEnv/Clean\ System\ Files/_system

echo first compile of system modules
for a in StdChar; # compile twice for inlining
do clean/exe/cocl -pt -pm -ou -P clean/StdEnv $a ;
done

echo second compile of system modules
for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
do clean/exe/cocl -pt -pm -ou -P clean/StdEnv $a ;
done

