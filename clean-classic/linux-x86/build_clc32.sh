set -e
# ./svn_checkout.sh clean-compiler/trunk compiler
./git_clone.sh clean-compiler-and-rts/compiler.git compiler

cd compiler
clm -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
unix/make.linux.sh
cd ..

