set -e
# ./checkout.sh libraries/ArgEnvUnix
if test ! -d libraries/ArgEnvUnix ; then
  ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
fi
# ./checkout.sh compiler
./svn_checkout.sh clean-compiler/branches/itask compiler

cd compiler
clm -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
cd unix
sed "s/-h 40M/-h 512m -s 16m/" <make.linux.sh >make.linux.sh_
rm -f make.linux.sh
mv make.linux.sh_ make.linux.sh
chmod +x make.linux.sh
cd ..
unix/make.linux.sh
cd ..

