#!/bin/sh
TARGET=target/clean-lib-argenv

export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH

cp -r src/clean-libraries-master/Libraries/ArgEnvUnix build/ArgEnv
(cd build/ArgEnv
	make -e
)

mkdir -p "$TARGET/lib/ArgEnv/Clean System Files"
for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README;
do  cp build/ArgEnv/$f $TARGET/lib/ArgEnv/$f
done
cp "build/ArgEnv/Clean System Files/ArgEnvC.o" "$TARGET/lib/ArgEnv/Clean System Files/"
