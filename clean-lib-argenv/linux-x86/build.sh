#!/bin/sh
TARGET=target/clean-lib-argenv

cp -r src/clean-libraries-master/Libraries/ArgEnvUnix build/ArgEnv
(cd build/ArgEnv
	cc -m32 -Wall -pedantic -O -c ArgEnvC.c
)

mkdir -p "$TARGET/lib/ArgEnv/Clean System Files"
for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README;
do  cp build/ArgEnv/$f $TARGET/lib/ArgEnv/$f
done
cp "build/ArgEnv/ArgEnvC.o" "$TARGET/lib/ArgEnv/Clean System Files/"
