#!/bin/sh
set -e
mkdir -p target/clean-lib-itasks

# Download node_modules from GitLab
(cd src/itasks-sdk-master
	curl -L -o node_modules.zip https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK/-/jobs/artifacts/master/download?job=npm
	unzip node_modules.zip
	rm node_modules.zip
)

# Add libraries
mkdir -p target/clean-lib-itasks/lib
cp -r src/itasks-sdk-master/Libraries target/clean-lib-itasks/lib/iTasks

# Add examples
mkdir -p target/clean-lib-itasks/examples/iTasks
cp -r src/itasks-sdk-master/Examples/* target/clean-lib-itasks/examples/iTasks/
find target/clean-lib-itasks -name "*.prj.default" | while read f; do
		mv "$f" "$(dirname $f)/$(basename -s .prj.default $f)".prj
	done

# Build tools
export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH

# Web collector
mkdir -p build/itasks-web-collector
mkdir -p target/clean-lib-itasks/lib/exe
cp -r src/itasks-sdk-master/Tools/WebResourceCollector.icl build/itasks-web-collector/
(cd build/itasks-web-collector
    clm -nr -nt -IL Generics -IL Platform WebResourceCollector -o itasks-web-collector 
)
cp build/itasks-web-collector/itasks-web-collector target/clean-lib-itasks/lib/exe/itasks-web-collector

# Create basicapiexamples aggregated module 
mkdir -p build/basicapiexamples
cp -r src/itasks-sdk-master/Examples/* build/basicapiexamples/
(cd build/basicapiexamples
    cat CreateBasicAPIExamples.prj.default > CreateBasicAPIExamples.prj
    cpm CreateBasicAPIExamples.prj
    ./CreateBasicAPIExamples.exe > BasicAPIExamples.icl
)
cp build/basicapiexamples/BasicAPIExamples.icl target/clean-lib-itasks/examples/iTasks/
#Remove create program
rm target/clean-lib-itasks/examples/iTasks/CreateBasicAPIExamples.icl
rm target/clean-lib-itasks/examples/iTasks/CreateBasicAPIExamples.prj

# Add environments
mkdir -p target/clean-lib-itasks/etc
cp src/itasks-sdk-master/Config/iTasks.prt target/clean-lib-itasks/etc/iTasks.prt
cp src/itasks-sdk-master/Config/linux-x86/iTasks.env target/clean-lib-itasks/etc/iTasks.env
