#!/bin/sh
set -e
# Set up
mkdir -p "test/clean"
mkdir -p "test/tests"
cp -r build/clean/* test/clean/
cp -r target/clean-lib-itasks/* test/clean/
cp -r src/itasks-sdk-master test/itasks-sdk
tail -n +3 test/clean/etc/iTasks.env >> test/clean/etc/IDEEnvs

# Compile and run
(cd "test"
 export CLEAN_HOME=`pwd`/clean
 export PATH=$CLEAN_HOME/bin:$PATH

	bash itasks-sdk/Tests/posix.sh
)
