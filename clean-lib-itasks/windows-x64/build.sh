#!/bin/sh
mkdir -p target/clean-lib-itasks

# Download node_modules from GitLab
(cd src/itasks-sdk-master
	curl -L -o node_modules.zip https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK/-/jobs/artifacts/master/download?job=npm
	unzip node_modules.zip
	rm node_modules.zip
)

# Add libraries
mkdir -p target/clean-lib-itasks/Libraries
cp -r src/itasks-sdk-master/Libraries target/clean-lib-itasks/Libraries/iTasks

# Add examples
mkdir -p target/clean-lib-itasks/Examples/iTasks
cp -r src/itasks-sdk-master/Examples/* target/clean-lib-itasks/Examples/iTasks/

# Rename all .prj.default files
for prj in `find target/clean-lib-itasks -name *.prj.default`; do
	mv $prj `echo $prj | sed s/.prj.default/.prj/`
done

# Build tools
export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH

# Web collector
mkdir -p build/itasks-web-collector
mkdir -p target/clean-lib-itasks/Tools/Clean\ System
cp src/itasks-sdk-master/Tools/WebResourceCollector.icl build/itasks-web-collector/
cp src/itasks-sdk-master/Tools/WebResourceCollector.prj.default build/itasks-web-collector/WebResourceCollector.prj

sed s/lib/Libraries/ src/itasks-sdk-master/Tools/WebResourceCollector.prj.default > build/itasks-web-collector/WebResourceCollector.prj
(cd build/itasks-web-collector
    ../../build/clean/cpm.exe WebResourceCollector.prj
)
cp build/itasks-web-collector/WebResourceCollector.exe target/clean-lib-itasks/Tools/Clean\ System/WebResourceCollector.exe

# Create basicapiexamples aggregated module 
mkdir -p build/basicapiexamples
cp -r src/itasks-sdk-master/Examples/* build/basicapiexamples/
(cd build/basicapiexamples
    sed s/lib/Libraries/ CreateBasicAPIExamples.prj.default > CreateBasicAPIExamples.prj
    ../../build/clean/cpm.exe CreateBasicAPIExamples.prj
    ./CreateBasicAPIExamples.exe > BasicAPIExamples.icl
)
cp build/basicapiexamples/BasicAPIExamples.icl target/clean-lib-itasks/Examples/iTasks/
#Remove create program
rm target/clean-lib-itasks/examples/iTasks/CreateBasicAPIExamples.icl
rm target/clean-lib-itasks/examples/iTasks/CreateBasicAPIExamples.prj

# Add environments
mkdir -p target/clean-lib-itasks/Config
cp src/itasks-sdk-master/Config/iTasks.prt target/clean-lib-itasks/Config/iTasks.prt
cp src/itasks-sdk-master/Config/windows-x64/iTasks.env target/clean-lib-itasks/Config/iTasks.env
