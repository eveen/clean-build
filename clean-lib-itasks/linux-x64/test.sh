#!/bin/sh
set -e

# Set up
export CLEAN_HOME=`pwd`/test/clean
export PATH=$CLEAN_HOME/bin:$PATH
cp -r target/clean-lib-itasks/* test/clean/
tail -n +3 test/clean/etc/iTasks.env >> test/clean/etc/IDEEnvs

#mkdir -p test
cp -r src/itasks-sdk-master test/itasks-sdk

# Compile and run
bash test/itasks-sdk/Tests/posix.sh
